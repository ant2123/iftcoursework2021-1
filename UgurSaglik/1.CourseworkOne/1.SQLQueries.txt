/*
UCL
Big Data in Quantitative Finance
Ugur Saglik
Coursework 1 (SQL)

/*

Query 1

Select * from equity_prices 
where volume>=100000000
GROUP By symbol_id

/*

Query 2

select price_id,close,volume,currency,cob_date 
from equity_prices 
where cob_date like '%Sep%' or cob_date like '%Nov%' 
order by price_id

SELECT symbol,security,GICSSector as Sector,GICSIndustry as Industry,price_id,close,volume,currency,cob_date FROM equity_static 
INNER JOIN equity_prices ON equity_static.symbol = equity_prices.symbol_id
WHERE currency='USD'
and volume>=10000000
and cob_date like '10-Nov-2020' 
Limit 52

SELECT symbol,security,GICSSector as Sector,GICSIndustry as Industry,price_id,close,volume,currency,cob_date 
FROM equity_static 
INNER JOIN equity_prices ON equity_static.symbol = equity_prices.symbol_id
Where currency='USD'
and volume>=10000000
and cob_date like '10-Sep-2020' 

Select AVG(volume),AVG(close),Count(volume),Count(close),max(volume),min(volume),max(close),min(close) 
From equity_prices 
WHERE cob_date like '10-Nov-2020' 
and volume >=10000000

Select AVG(volume),AVG(close),Count(volume),Count(close),max(volume),min(volume),max(close),min(close)
From equity_prices 
WHERE cob_date like '10-Sep-2020' 
and volume >=10000000

/*

Query 3

select limit_id,trader_id as trader,limit_type,limit_amount,currency,limit_start, limit_end
from trader_limits 
where limit_start like '%Sep%' 
and limit_end is not NULL
and trader='DHB1075' 

select limit_id,trader_id as trader,limit_type,limit_amount,currency,limit_start 
from trader_limits 
where limit_start
like '%Nov%' 
and trader='DHB1075' 