#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Zhi Lin
# Coursework : Coursework3
# Use Case   : Case 3a
# File       : App.R
#--------------------------------------------------------------------------------------
# Import Libraries
library(DT)
library(shinydashboard)
library(shinythemes)
library(ggplot2)
library(lubridate)
library(dplyr)
library(RSQLite)   
library(mongolite)
library(dplyr)
library(tibble)
library(shiny)
library(lubridate)
options(scipen = 999)

# Parse ARGs ----------------------------------------------------------------------
#Args = c("C:/Users/Patrick_Lin/Documents/iftcoursework2021","script.config", "script.params","ui.R","server.R")
Args <- commandArgs(TRUE)

# Set working directory
setwd(Args[1])

# Source config
source(paste0("./ZhiLin/3.CourseworkThree/config/",Args[2]))
# Source params
source(paste0("./ZhiLin/3.CourseworkThree/config/",Args[3]))
# Source ui.R
source(paste0("./ZhiLin/3.CourseworkTHree/src/",Args[4]))
# Source server.R
source(paste0("./ZhiLin/3.CourseworkTHree/src/",Args[5]))



# Create SQL Database connection
ConSql <- dbConnect(RSQLite::SQLite(), DBConnection$SQLDataBase$Connection)
# create MongoDB connection
ConMongo <- mongo(DBConnection$MongoDataBase[[1]],DBConnection$MongoDataBase[[2]],DBConnection$MongoDataBase[[3]])

# Store data tables from sqlite in a dataframe structure into R
Portfolio_Position <- RSQLite::dbGetQuery(ConSql, "SELECT * FROM portfolio_positions")
equityStatic <- RSQLite::dbGetQuery(ConSql, "SELECT * FROM equity_static")
equity_prices <- RSQLite::dbGetQuery(ConSql, "SELECT * FROM equity_prices")

# The time string should be modified to pair two dataframes
Portfolio_Position$cob_date <- lubridate::dmy(Portfolio_Position$cob_date)
Portfolio_Position$cob_date <- format(Portfolio_Position$cob_date,'%Y-%m-%d')

equity_prices$cob_date <- lubridate::dmy(equity_prices$cob_date)
equity_prices$cob_date <- format(equity_prices$cob_date,'%Y-%m-%d')

# Calculation of all trades on 11-Nov-2021 and put into a dataframe of Trade_11
Trade_Nov11 <- ConMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-11T00:00:00.000Z)\", \"$lt\": \"ISODate(2021-11-12T00:00:00.000Z)\"}}")
Trade_Nov11$DateTime <- as.Date("2021-11-11")
Trade_11<- aggregate(cbind(Trade_Nov11$Quantity,Trade_Nov11$Notional), list(Trade_Nov11$DateTime,Trade_Nov11$Trader,Trade_Nov11$Symbol,Trade_Nov11$Ccy),sum)

Trade_11 <- as.data.frame(append(Trade_11, list(pos_id = NA), after = 0))

colnames(Trade_11) <- c("pos_id", "cob_date","trader","symbol","ccy","net_quantity","net_amount") 
Trade_11$pos_id <- paste(Trade_11$trader,"20211111", Trade_11$symbol,sep="")

Trade_11$cob_date <- format(Trade_11$cob_date,'%Y-%m-%d')


# Calculation of all trades on 12-Nov-2021 and put into a dataframe of Trade_12
Trade_Nov12 <- ConMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-12T00:00:00.000Z)\", \"$lt\": \"ISODate(2021-11-13T00:00:00.000Z)\"}}")
Trade_Nov12$DateTime <- (as.Date("2021-11-12"))

Trade_12<- aggregate(cbind(Trade_Nov12$Quantity,Trade_Nov12$Notional),list(Trade_Nov12$DateTime,Trade_Nov12$Trader,Trade_Nov12$Symbol,Trade_Nov12$Ccy),sum)

Trade_12 <- as.data.frame(append(Trade_12, list(pos_id = NA), after = 0))

colnames(Trade_12) <- c("pos_id", "cob_date","trader","symbol","ccy","net_quantity","net_amount") 
Trade_12$pos_id <- paste(Trade_12$trader,"20211112", Trade_12$symbol,sep="")

Trade_12$cob_date <- format(Trade_12$cob_date,'%Y-%m-%d')


# Calculate Total Portfolio Positions
portfolio11 <- rbind(Portfolio_Position,Trade_11)
PortfolioPosition <- rbind(portfolio11,Trade_12)

PortfolioPosition <- as.data.frame(append(PortfolioPosition,list(GICSSector = NA),after = 4))

for(row1 in 1:length(PortfolioPosition[[1]])){
  for(row2 in 1:length(equityStatic[[1]])){
    if(PortfolioPosition[row1,'symbol'] == equityStatic[row2,'symbol']){
      PortfolioPosition[row1,'GICSSector'] <- equityStatic[row2,'GICSSector']
    }
  }
}


# Use close price to compute Mark-to-Market in the calculation of Exposures
equity_prices <- equity_prices[,c(5,8,9)]
colnames(equity_prices) <- c('close','cob_date','symbol')
PortfolioPosition <- merge(PortfolioPosition,equity_prices,by=c('cob_date','symbol'))
PortfolioPosition$net_amount <- PortfolioPosition$net_quantity * PortfolioPosition$close
PortfolioPosition$close <- NULL

# Calculation of exposures for each GICSSector
Portfolio_Agg <- aggregate(cbind(PortfolioPosition$net_quantity,PortfolioPosition$net_amount), 
                           list(PortfolioPosition$cob_date,PortfolioPosition$trader,PortfolioPosition$GICSSector),sum)
colnames(Portfolio_Agg) <- c('cob_date','trader','GICSSector','net_quantity','net_amount')

# Aggregate and sum up the net-amount for each trader
sum_Portfolio_Agg <- aggregate(PortfolioPosition$net_amount,list(PortfolioPosition$cob_date,PortfolioPosition$trader),sum)
colnames(sum_Portfolio_Agg) <- c('cob_date','trader','net_amount')
Portfolio_Agg <- as.data.frame(append(Portfolio_Agg, list(exposures = NA)))

for(row1 in 1:length(Portfolio_Agg[[1]])){
  for(row2 in 1:length(sum_Portfolio_Agg[[1]])){
    if(Portfolio_Agg[row1,'cob_date']==sum_Portfolio_Agg[row2,'cob_date'] && Portfolio_Agg[row1,'trader']==sum_Portfolio_Agg[row2,'trader']){
      Portfolio_Agg[row1,'exposures'] <- Portfolio_Agg[row1,'net_amount']/sum_Portfolio_Agg[row2,'net_amount']
    }
  }
}

# Duplicate elements/rows are eliminated
trader <- unique(PortfolioPosition$trader)
cob_date <- unique(PortfolioPosition$cob_date)

# Display the shiny-dashboard interface
shinyApp(ui, server)

