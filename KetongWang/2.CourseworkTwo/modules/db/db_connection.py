import configparser
import sqlite3
import pymongo
import os


BaseDir = os.path.dirname(os.path.abspath(__file__))
config = configparser.ConfigParser()
config.optionxform = str
config.read(os.path.join(BaseDir, '../../config/config.ini'), encoding="utf-8")

db_path=config.get("sqlite", "db_path")

host=config.get("mongodb", "host")
port=config.get("mongodb", "port")
dbname=config.get("mongodb", "dbname")
collection=config.get("mongodb", "collection")

"""
Get database connection objects: including mongodb and sqlite3
"""

def get_sqlite_connection():
    """
    Get the splite3 connection object
    :return:connection
    """
    # connect to Equity.db
    path=os.path.join(BaseDir, '../../'+db_path)
    print(path)
    conn = sqlite3.connect(path)
    return conn

def get_mongo_connection():
    """

    :param host: mongodb host
    :param port: mongodb port
    :param dbname: database name
    :param collection_nam: collection name
    :return:
    """
    myclient = pymongo.MongoClient("mongodb://{}:{}/".format(host,port))
    # choose database
    mydb = myclient[dbname]
    # Choose the given collection
    collections = mydb[collection]
    return collections








