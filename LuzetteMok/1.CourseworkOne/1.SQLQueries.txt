-- GROUP BY query with functions ROUND, AVG, WHERE conditions and sorted output in order
SELECT equity_static.symbol, equity_prices.volume, equity_prices.cob_date, equity_prices.close, ROUND(AVG(equity_prices.close), 2) as avg_close
FROM equity_prices
JOIN equity_static on equity_prices.symbol_id = equity_static.symbol
WHERE close > 160
GROUP by symbol
ORDER BY close DESC
LIMIT 10;

-- JOIN statement with WHERE condition and basic arithmetic
SELECT portfolio_positions.trader, portfolio_positions.symbol, equity_prices.cob_date, equity_prices.close, 
equity_prices.close * portfolio_positions.net_quantity as current_value,
equity_prices.close * portfolio_positions.net_quantity - portfolio_positions.net_amount as pnl
FROM portfolio_positions
JOIN equity_prices on portfolio_positions.symbol = equity_prices.symbol_id
WHERE equity_prices.cob_date = '12-Nov-2021'
