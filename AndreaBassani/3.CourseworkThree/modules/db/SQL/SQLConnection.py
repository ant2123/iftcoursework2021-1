
import sqlite3
from sqlite3 import Error
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from utility.Utility import get_config

def SQL_connection():
    con = None 
    try:
        con = sqlite3.connect(get_config()[0])
        return con
    except Error as e:
        print(e)
    return con


# %%
