#%%
from xml.dom.pulldom import START_DOCUMENT
import pandas as pd 
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from dateconvertor.DateConvertor import dateConvertor
from MongoDB.MongoDBConnection import MongoDB_connection
from utility.Utility import get_config

def MongoDB_hystorical(traderId):
    
    col = MongoDB_connection()   
    documents = col.find({"Trader":{"$eq":traderId}}) 
    documents = pd.DataFrame(documents, columns =['DateTime','TradeId','Trader','Symbol','Quantity','Notional','TradeType','Ccy', 'Counterparty'])
    documents['DateTime'] = documents['DateTime'].map(lambda x: x[8:18])
    documents['DateTime'] = pd.to_datetime(documents['DateTime'], format='%Y-%m-%d')
    return documents

def start_date():
    date_start = str(MongoDB_hystorical(get_config()[5])['DateTime'].min())
    return date_start[:10]

startdate = start_date()

def MongoDB_documents_btwDates(endate, traderID):

    conv_date_start = dateConvertor(startdate)
    conv_date_end = dateConvertor(endate)
    col = MongoDB_connection()   
    #Find the data for the selected period
    documents = col.find({"$and":[{"DateTime":{"$gt":conv_date_start[0][0],"$lt":conv_date_end[0][1]}},{"Trader":{"$eq":traderID}}]}) 
    documents = pd.DataFrame(documents, columns =['DateTime','TradeId','Trader','Symbol','Quantity','Notional','TradeType','Ccy', 'Counterparty'])
    documents['DateTime'] = documents['DateTime'].map(lambda x: x[8:18])
    documents['DateTime'] = pd.to_datetime(documents['DateTime'], format='%Y-%m-%d')
    return documents






#MongoDB_hystorical('MRH5231')
#MongoDB_documents_btwDates('2020-11-11', '2021-11-11', 'MRH5231')

# %%
