#%%
import pandas as pd
import matplotlib.pyplot as plt
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from analysis.SimpleReturn import simple_return
from db.MongoDB.MongoDBQuery import MongoDB_documents_btwDates
from analysis.Benchmark import comulative_benchmark_return
from db.utility.DateFile import end_date
from db.utility.Utility import get_config
from db.MongoDB.MongoDBQuery import start_date

traderId = get_config()[5]
enddate = end_date()
startdate =start_date()


def stock_comulative_return():
    stock_daily_return = simple_return()[0].fillna(0)
    stock_commulative_return = (1 + stock_daily_return).cumprod()-1
    stock_commulative_return = stock_commulative_return.reset_index()
    return stock_commulative_return

def portfolio_comulative_quantity():
    trades = MongoDB_documents_btwDates(enddate, traderId)
    trades.groupby(['DateTime','Symbol'], as_index=False).sum()
    trades_quantity = trades.pivot_table(index=stock_comulative_return()['cob_date'], columns = 'Symbol', values = ['Quantity'],
    fill_value=0)
    trades_quantity.columns = [col[1] for col in trades_quantity.columns.values]
    trades_quantity = trades_quantity.cumsum()
    return trades_quantity

def portfolio_comulative_weight():
    trades_quantity = portfolio_comulative_quantity().reset_index()
    stock_price = simple_return()[1].reset_index()
    stock_price = stock_price[portfolio_comulative_quantity().reset_index().columns.values][1:].reset_index()
    total_trades_value = trades_quantity.iloc[:,1:].mul(stock_price.iloc[:,1:])
    trades_weight = total_trades_value.iloc[:,:-1].div(total_trades_value.iloc[:,:-1].sum(axis=1), axis=0)
    trades_weight.insert(0,'cob_date', trades_quantity['cob_date'])  
    return trades_weight

def portfolio_comulative_return():
    trades_weight = portfolio_comulative_weight()
    stock_commulative_return = stock_comulative_return()
    portfolio_return_matrix = trades_weight.iloc[:,1:].mul(stock_commulative_return[trades_weight.columns.values[1:]])
    portfolio_return_matrix.insert(0,'cob_date', trades_weight['cob_date']) 
    return portfolio_return_matrix

def portfolio_vs_benchmark():
    benchmark = comulative_benchmark_return()
    portfolio = pd.DataFrame(portfolio_comulative_return().iloc[:,1:].sum(axis=1))
    portfolio['Date'] = portfolio_comulative_return()['cob_date']
    table = pd.concat([portfolio.set_index('Date'), benchmark.set_index('Date')], axis = 1, join='inner')
    table = table.reset_index()
    table = table.set_axis(['Date', 'Portfolio', 'SPY'], axis=1)
    return table

#xxx = portfolio_comulative_return()
# %%
