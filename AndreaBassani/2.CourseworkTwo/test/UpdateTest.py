import modules.UpdatePortfolioPosition.UpdateSQL as uu
import modules.db.MongoDB.Query as mq
import pandas as pd 


def TestTradesNumber (date):
    update = uu.updatePortfolioPosition(date)
    registered = mq.MongoDB_documents_btwDates(date)
    if update == range(len(registered)):
        print("test ok")
    else:
        print(f"There are: {len(registered)} register trades but equity_portfolio has been updated with: {update}")