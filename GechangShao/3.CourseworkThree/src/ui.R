#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Gechang Shao
# Topic   : Create shiny page - ui
#--------------------------------------------------------------------------------------

shinyUI <- dashboardPage(
  skin = "purple",#set purple skin of dashboard
  dashboardHeader(title = "Portfolio Positions",
                  titleWidth = 300),
  dashboardSidebar(

# Define the way to choose the trader -------------------------------------

    selectInput(
      inputId = "trader",
      label = strong("Trader"),
      choices = unique(PositionsWithIndustry$trader_name),
      selected = "John Black"
    ),

# Define the way to choose the date ---------------------------------------

    dateInput(
      "date",
      strong("Date"),
      value = "2021-11-10",
      min = "2020-01-03",
      max = "2021-11-10"
    )
  ),

# Define the output format ------------------------------------------------

  dashboardBody(
    tags$head(tags$style(
      HTML(
        '
      .main-header .logo {
        font-family: "Georgia", Times, "Times New Roman", serif;
        font-weight: bold;
        font-size: 24px;
      }
    '
      )
    )),
    h2(textOutput("TableOneCaption")),
    dataTableOutput("TableOne"),
    h2(textOutput("TableTwoCaption")),
    dataTableOutput("TableTwo"),
    plotOutput("pie")
  )
)
