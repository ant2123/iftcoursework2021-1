from sqlalchemy import create_engine
import configparser
from pymongo import MongoClient
import os

config = configparser.ConfigParser()
config_path = os.path.realpath('..')
FileName = 'config\script.ini'
completeName = os.path.join(config_path, FileName)

def SQLite():
    file = completeName
    config = configparser.ConfigParser()
    config.read(file)
    GITRepoDirectory = config.get("Database", "sql")
    engine = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
    engine = engine.connect()
    return engine

def MongoDB():
    file = completeName
    config = configparser.ConfigParser()
    config.read(file)
    cluster = MongoClient(config.get("Database", "cluster"))
    db = cluster[config.get("Database", "db")]
    collection = db[config.get("Database", "collection")]
    return collection


