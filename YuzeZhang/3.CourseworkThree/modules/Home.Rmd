---
title: "Coursework 3"
author: "Yuze Zhang"
date: "2022/1/27"
output:
  html_document: default
  fig_width: 30
  fig_height: 20
---

# Introduction

Front office is asking to provide with an analytics report with the details of main allocations and historical trends for a given trader. In order to achieve this objective, we select the trader "JBX1566" and we generate a report for end of day 2021-Nov-12. 

This html report will be created in order to provide useful information to monitor the trading activity of the trader "JBX1566". 


# Useful information to monitor the trader's trading activity

## Portfolio overview and its weights
For example, we can provide information about the stocks that the trader "JBX1566" invests in and alongside with their weights to monitor the trader's trading activity.

Calculating the weight of a portfolio can be a very useful investment tool. Since the weight of a portfolio can tell investors how much of their portfolio performance is dependent on a particular asset. For instance, if you have an asset that makes up 70% of your portfolio weight, then it will be more vital to the success of your portfolio than an asset that makes up only about 3%. Meanwhile, a canny trader might sell a stock that has gained and reinvest the proceeds to bring the portfolio back to its correct balance.


The following diagram shows the stock portfolio for the trader "JBX1566" on 2021-Nov-12 and the respective portfolio weights.

### Portfolio overview on 2021-NOV-12
```{r echo=FALSE, Warning=FALSE}

# Import libraries
library(mongolite)
library(RSQLite)
library(dplyr)

# Setting SQL connection
equitydb <- dbConnect(SQLite(), "/Users/shangli/Desktop/coursework_3/iftcoursework2021/000.DataBases/SQL/Equity.db")


# Setting MongoDB connection
conMongo <- mongo(collection = "Coursework3", db = "admin", url="mongodb://127.0.0.1:27017/", verbose = FALSE,options = ssl_options())
fullUniverse <- conMongo$find(query = "{}") 

# Then, we retrieve all trades as per end of day (2021-11-12) and select one specific trader (JBX1566)
mongo_trades12 <- conMongo$find('{ "DateTime" : { "$regex" : ".2021-11-12.", "$options" : "i" }  }')
Trader12 <- subset(mongo_trades12, Trader == "JBX1566")
df12 <- Trader12
df12$cob_date = "2021-11-12"
AggTrade_12 <-aggregate(cbind(Quantity,Notional)~Trader+Symbol+Ccy+cob_date, data=df12, FUN=sum)

```

#### Table
```{r echo=FALSE, Warning=FALSE}
library(knitr)
knitr::kable(AggTrade_12)
```

#### Visualization
```{r echo=FALSE, Warning=FALSE}
plot(AggTrade_12$Quantity, AggTrade_12$Notional)
```
                                                                                                                        
### Portfolio allocation for Trader JBX1566 on 2021-Nov-12
```{r echo = FALSE}
# Then, I could calculate the portfolio weights for the trader's portfolio
AggTrade_12$Sum <- sum(AggTrade_12$Notional)
AggTrade_12$Weight <- AggTrade_12$Notional / AggTrade_12$Sum


# Then, we need to calculate the return of the trader's portfolio
# In order to calculate the return, we need to find the prices
price12 <- dbGetQuery(equitydb, paste0("SELECT * FROM equity_prices WHERE cob_date = '12-Nov-2021'"))
colnames(AggTrade_12)[colnames(AggTrade_12) == "Symbol"] <- "symbol_id"
dataframe_merge = merge(AggTrade_12, price12, by="symbol_id")
dmerge <- dataframe_merge
dmerge$Sum <- sum(dmerge$Notional)
dmerge$weights <- dmerge$Notional / dmerge$Sum

```

#### Table
```{r echo = FALSE}
knitr::kable(AggTrade_12)
```

#### Visualization - Portfolio weights on 2021-Nov-12
```{r echo = FALSE}
 
barplot(dmerge$weights,
main = "Portfolio weights overview on 2021-Nov-12",
xlab = "Stock",
ylab = "Portfolio Weight",
names.arg = dmerge$symbol_id,
col = "orange",
border = "orange",
cex.names = 0.25)
```


---

Besides portfolio weights, we could also plot the return of the portfolio to see the gain or loss realized by each stock to monitor the trading activity. If the trader finds his or her portfolio is not meeting the expected return, or that losses ar falling outstide an acceptable range, then the trader may need to adjust their portfolio invesetments.

Here, we first calculate the one-day return for each stock for the end of day 2021-Nov-12 in the trader JBX1566's portfolio.


## One-day return for each stock in trader JBX1566's portfolio
```{r echo=FALSE}
# Then, we can calculate the one-day return for 2021-11-12:
price11 <- dbGetQuery(equitydb, paste0("SELECT * FROM equity_prices WHERE cob_date = '11-Nov-2021'"))
dataframe_merge2 = merge(AggTrade_12, price11, by="symbol_id")
dmerge$close_11 <- dataframe_merge2$close

# So, the one-day return would be:
dmerge$Return <- (dmerge$close-dmerge$close_11)/dmerge$close_11

Symbol <- dmerge$symbol_id
DayReturn <- dmerge$Return
df17 <- data.frame(Symbol, DayReturn)
```

### Table
```{r echo=FALSE}
knitr::kable(df17)
```

### Horizontal Bar plot

```{r echo=FALSE}
barplot(dmerge$Return, 
main = "One-day return for each stock in trader JBX1566's portfolio",
xlab = "Return",
ylab = "Stock",
names.arg = c("AIG","AMZN","ANSS","AON","APTV","CBRE","CMCSA","EFX", "GD", "HBAN","HES","HOLX","HSIC","HSY","IP","JWN","MLM","MSI","NLSN","PFE","PHM","PLD","PGRO","TWTR","UHS","UNP","UPS","WAT","WU","XLNX"),
col = "orange",
border = "orange",
horiz = TRUE,
cex.names = 0.125)
```

### The expected one-day return for End of day portfolio:
```{r echo=FALSE}
ExpectedReturn <- sum(dmerge$Return*dmerge$weights)
ExpectedReturn
```

### The End of Day portfolio variance:
```{r echo=FALSE}
# The End Day portfolio variance would be:
variance <- sum(dmerge$weights*(dmerge$Return-ExpectedReturn)^2)
variance
```

### The End of Day portfolio standard deviation:
```{r echo=FALSE}
# The End Day portfolio variance would be:
std <- sqrt(variance)
std
```

### Summary of risk metrics for End of Day portfolio:
```{r echo=FALSE}
metrics <- c('Expected one-day return', 'Portfolio variance', 'Portfolio standard deviation')
portfolio_JBX1566 <- c(0.0138074370656677, 2.3477718990789e-05, 0.00484538120180332)
df0 <- data.frame(metrics,portfolio_JBX1566)
knitr::kable(df0)
```
---

## Historical expected returns
For instance, below is a table which summarizes the information about the historical return based on day 12's portfolio allocation (in this case, we assume the portfolio weights are unchanged)

### Table
```{r echo=FALSE}
# We could also compute the historical one-day return as follows:
SQLQuery12 <- paste0("SELECT * FROM equity_prices WHERE symbol_id IN ('AIG','AMZN','ANSS','AON','APTV','CBRE','CMCSA','EFX','GD','HBAN','HES','HOLX','HSIC','HSY','IP','JWN','MLM','MSI','NLSN','PFE','PHM','PLD','PRGO','TWTR','UHS','UNP','UPS','WAT','WU','XLNX')")
stockprice <- dbGetQuery(equitydb, SQLQuery12)

stockprice11 <- select(stockprice, -c(price_id, open, high, low, volume, currency, cob_date))
matrix <- data.matrix(stockprice11)

# Then, we can calculate the covariance matrix:
stockprice11$AIG <- filter(stockprice11, symbol_id == 'AIG')$close
stockprice11$AMZN <- filter(stockprice11, symbol_id == 'AMZN')$close
stockprice11$ANSS <- filter(stockprice11, symbol_id == 'ANSS')$close
stockprice11$AON <- filter(stockprice11, symbol_id == 'AON')$close
stockprice11$APTV <- filter(stockprice11, symbol_id == 'APTV')$close
stockprice11$CBRE <- filter(stockprice11, symbol_id == 'CBRE')$close
stockprice11$CMCSA <- filter(stockprice11, symbol_id == 'CMCSA')$close
stockprice11$EFX <- filter(stockprice11, symbol_id == 'EFX')$close
stockprice11$GD <- filter(stockprice11, symbol_id == 'GD')$close
stockprice11$HBAN <- filter(stockprice11, symbol_id == 'HBAN')$close
stockprice11$HES <- filter(stockprice11, symbol_id == 'HES')$close
stockprice11$HOLX <- filter(stockprice11, symbol_id == 'HOLX')$close
stockprice11$HSIC <- filter(stockprice11, symbol_id == 'HSIC')$close
stockprice11$HSY <- filter(stockprice11, symbol_id == 'HSY')$close
stockprice11$IP <- filter(stockprice11, symbol_id == 'IP')$close
stockprice11$JWN <- filter(stockprice11, symbol_id == 'JWN')$close
stockprice11$MLM <- filter(stockprice11, symbol_id == 'MLM')$close
stockprice11$MSI <- filter(stockprice11, symbol_id == 'MSI')$close
stockprice11$NLSN <- filter(stockprice11, symbol_id == 'NLSN')$close
stockprice11$PFE <- filter(stockprice11, symbol_id == 'PFE')$close
stockprice11$PHM <- filter(stockprice11, symbol_id == 'PHM')$close
stockprice11$PLD <- filter(stockprice11, symbol_id == 'PLD')$close
stockprice11$PRGO <- filter(stockprice11, symbol_id == 'PRGO')$close
stockprice11$TWTR <- filter(stockprice11, symbol_id == 'TWTR')$close
stockprice11$UHS <- filter(stockprice11, symbol_id == 'UHS')$close
stockprice11$UNP <- filter(stockprice11, symbol_id == 'UNP')$close
stockprice11$UPS <- filter(stockprice11, symbol_id == 'UPS')$close
stockprice11$WAT <- filter(stockprice11, symbol_id == 'WAT')$close
stockprice11$WU <- filter(stockprice11, symbol_id == 'WU')$close
stockprice11$XLNX <- filter(stockprice11, symbol_id == 'XLNX')$close

matrix_all <- select(stockprice11, -c(close, symbol_id))

AIG <- diff(log(stockprice11$AIG),1)
AMZN <- diff(log(stockprice11$AMZN),1)
ANSS <- diff(log(stockprice11$ANSS),1)
AON <- diff(log(stockprice11$AON),1)
APTV <- diff(log(stockprice11$APTV),1)
CBRE <- diff(log(stockprice11$CBRE),1)
CMCSA <- diff(log(stockprice11$CMCSA),1)
EFX <- diff(log(stockprice11$EFX),1)
GD <- diff(log(stockprice11$GD),1)
HBAN <- diff(log(stockprice11$HBAN),1)
HES <- diff(log(stockprice11$HES),1)
HOLX <- diff(log(stockprice11$HOLX),1)
HSIC <- diff(log(stockprice11$HSIC),1)
HSY <- diff(log(stockprice11$HSY),1)
IP <- diff(log(stockprice11$IP),1)
JWN <- diff(log(stockprice11$JWN),1)
MLM <- diff(log(stockprice11$MLM),1)
MSI <- diff(log(stockprice11$MSI),1)
NLSN <- diff(log(stockprice11$NLSN),1)
PFE <- diff(log(stockprice11$PFE),1)
PHM <- diff(log(stockprice11$PHM),1)
PLD <- diff(log(stockprice11$PLD),1)
PRGO <- diff(log(stockprice11$PRGO),1)
TWTR <- diff(log(stockprice11$TWTR),1)
UHS <- diff(log(stockprice11$UHS),1)
UNP <- diff(log(stockprice11$UNP),1)
UPS <- diff(log(stockprice11$UPS),1)
WAT <- diff(log(stockprice11$WAT),1)
WU <- diff(log(stockprice11$WU),1)
XLNX <- diff(log(stockprice11$XLNX),1)

portfolio.r <- data.frame(AIG, AMZN, ANSS, AON, APTV, CBRE, CMCSA, EFX, GD, HBAN, HES, HOLX, HSIC, HSY, IP, JWN, MLM, MSI, NLSN, PFE, PHM, PLD, PRGO, TWTR, UHS, UNP, UPS, WAT, WU, XLNX)

# Then, the expected portfolio one-day return would be:
portfolio_weight <- as.vector(dmerge$weights)
Statistics <- summarize_all (portfolio.r, mean)
R <- Statistics * portfolio_weight

knitr::kable(R)

```

### Visualization
```{r echo=FALSE, Warning=FALSE}
Returns = c(-3.517996e-08,-3.000754e-05,-8.258589e-07,-1.867175e-07,-1.665393e-07,-1.93416e-08,-6.058756e-08,-1.621523e-07, -3.247537e-07, -8.946414e-09,-1.607424e-07,-8.596809e-08,-2.004339e-08,-6.422447e-09,-1.354399e-08,3.126129e-08,-1.791914e-06,-1.88648e-07,-8.501214e-10,-1.633197e-07,-7.4998e-08,-1.986192e-07,1.869638e-08,-8.7159e-08,3.755718e-09,-7.048513e-07,-8.814739e-08,-2.032009e-06,5.69646e-09,-5.523619e-08)
barplot(Returns, 
main = "One-day Historical return for each stock in trader JBX1566's portfolio",
xlab = "Historical Return",
ylab = "Stock",
names.arg = c("AIG","AMZN","ANSS","AON","APTV","CBRE","CMCSA","EFX", "GD", "HBAN","HES","HOLX","HSIC","HSY","IP","JWN","MLM","MSI","NLSN","PFE","PHM","PLD","PGRO","TWTR","UHS","UNP","UPS","WAT","WU","XLNX"),
col = "orange",
border = "orange",
cex.names = 0.125)
```



---
We could also compute a table which summarizes the information about the portfolio for the trader JBX1566 as follows (which is, the main portfolio metrics in terms of risk and performance of the trader portfolio:
```{r echo=FALSE, Warning=FALSE}
df <- data.frame(t(portfolio_weight))
variance <- data.frame(t(portfolio_weight))*cov(portfolio.r)*portfolio_weight

# As a result, the standard deviation is equal to the square root of the portfolio variance, that is:
std <- sqrt(sum(variance))

# Then, the historical expected portfolio one-day return would be:
Statistics <- summarize_all (portfolio.r, mean)
R <- Statistics * portfolio_weight

# We can also calculate the expected annual return for the portfolio:
dataframe <- subset(stockprice, cob_date == "12-Nov-2021")
dataframe1 <- subset(stockprice, cob_date == "12-Nov-2020")
dataframe2 = merge(dataframe, dataframe1, by="symbol_id")
dataframe2$return <- (dataframe2$close.x-dataframe2$close.y)/dataframe2$close.y
dataframe2$weight <- dmerge$weights
dataframe2$expected <- dataframe2$return * dataframe2$weight
annual_return <- sum(dataframe2$expected)

# Summary of the portfolios
Metrics <- c('Average daily return', 'Annual return', 'Portfolio variance', 'Portfolio standard deviation')
JBX1566 <- c(-3.741068e-05, 0.2348696, 0.0001396923, 0.01181915)
df1 <- data.frame(Metrics,JBX1566)

kable(head(df1), caption = "Summary of risk metrics for the End of Day portfolio based on historical data")
```



