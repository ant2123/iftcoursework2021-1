# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 18:49:09 2021

@author: Haris
"""


#%% Import packages

import sqlite3
import pandas as pd
from pymongo import MongoClient

#%% Connect to sql database

Equity_sql = "D:/Haris Qureshi/UNI/UCL/IFT/Year 2/Big Data/Packages/Git/exam/iftcoursework2021/000.DataBases/SQL/Equity.db"
con_sql = sqlite3.connect(Equity_sql)
cur = con_sql.cursor()

#%% test sql connection

equity_prices = pd.read_sql_query("SELECT * from equity_prices", con_sql)

print(equity_prices.info())

#%% Connect to mongodb

con_mongo = MongoClient('mongodb://localhost')
db = con_mongo.Equity
coll = db.CourseworkTwo

#%% test mongo connection

Mongo_data = coll.find({})
Mongo_data = pd.DataFrame(Mongo_data)

Mongo_data.head()
Mongo_data.info()

#%% Retrieve all trades as per end of day 11th and 12th November 2021

mongo_trades_11 = pd.DataFrame(coll.find({"DateTime": {"$gt": "ISODate(2021-11-11T00:00:00.000Z)", "$lte": "ISODate(2021-11-12T00:00:00.000Z)"}}))
mongo_trades_12 = pd.DataFrame(coll.find({"DateTime": {"$gt": "ISODate(2021-11-12T00:00:00.000Z)", "$lte": "ISODate(2021-11-13T00:00:00.000Z)"}}))
                               
mongo_trades_11.info()
mongo_trades_12.info()


#%% Prices from sql on the 11th and 12th Nov

sql_prices_11 = pd.read_sql_query("SELECT low, high, symbol_id, cob_date from equity_prices WHERE cob_date = '11-Nov-2021' ", con_sql)
sql_prices_12 = pd.read_sql("SELECT low, high, symbol_id, cob_date from equity_prices WHERE cob_date = '12-Nov-2021' ", con_sql)

print(sql_prices_11.head())
print(sql_prices_12.head())

#%% Trade prices from mongo, notional/quantity 

mongo_trades_11['TradePrice'] = ((mongo_trades_11["Notional"])/(mongo_trades_11["Quantity"]))
mongo_trades_12['TradePrice'] = ((mongo_trades_12["Notional"])/(mongo_trades_12["Quantity"]))

print(mongo_trades_11)
print(mongo_trades_11)


#%% Merge mongo and sql data

merge_11 = pd.merge(mongo_trades_11, sql_prices_11, left_on='Symbol',right_on='symbol_id')
merge_12 = pd.merge(mongo_trades_12, sql_prices_12, left_on='Symbol',right_on='symbol_id')

print(merge_11)
print(merge_12)

merge_11_12 = pd.concat([merge_11, merge_12])
print(merge_11_12.info())

#%% Extract suspicious tardes
     
Suspicious_trades = merge_11_12.loc[(merge_11_12['TradePrice'] > merge_11_12['high']) | (merge_11_12['TradePrice'] < merge_11_12['low'])]     
Suspicious_trades.index = range(len(Suspicious_trades))
Suspicious_trades

#%% Create a table in SQLite called trades_suspects

def SQLtrades_suspectsTable():
    trades_suspects_table = "CREATE TABLE trades_suspects (\
    TradeId TEXT PRIMARY KEY,\
    DateTime TEXT NOT NULL,\
    Trader TEXT NOT NULL,\
    Symbol TEXT NOT NULL,\
    DayLow INTEGER,\
    DayHigh INTEGER,\
    TradePrice INTEGER,\
    Quantity INTEGER,\
    Notional INTEGER,\
    Counterparty TEXT NOT NULL,\
    FOREIGN KEY (Trader) REFERENCES equity_prices(symbol_id))"
    return (trades_suspects_table)

con_sql.execute(SQLtrades_suspectsTable())

#%% To delete and reconfigure table 

def SQLDeleteTable():
    return("DROP TABLE trades_suspects")
con_sql.execute(SQLDeleteTable())

#%% Insert suspicious trades into sql table

Equity_sql = "D:/Haris Qureshi/UNI/UCL/IFT/Year 2/Big Data/Packages/Git/exam/iftcoursework2021/000.DataBases/SQL/Equity.db"
con_sql = sqlite3.connect(Equity_sql)
cur = con_sql.cursor()

for i in range(len(Suspicious_trades)):
    cur.execute(f'INSERT INTO trades_suspects (TradeId, DateTime, Trader, Symbol, DayLow, DayHigh, TradePrice, Quantity, Notional, Counterparty) \
              VALUES("{Suspicious_trades.loc[i,"TradeId"]}",\
              "{Suspicious_trades.loc[i,"DateTime"]}",\
              "{Suspicious_trades.loc[i,"Trader"]}",\
              "{Suspicious_trades.loc[i,"Symbol"]}",\
              {Suspicious_trades.loc[i,"low"]},\
              {Suspicious_trades.loc[i,"high"]},\
              {Suspicious_trades.loc[i,"TradePrice"]},\
              {Suspicious_trades.loc[i,"Quantity"]},\
              {Suspicious_trades.loc[i,"Notional"]},\
              "{Suspicious_trades.loc[i,"Counterparty"]}")' )
                       
con_sql.commit()

#%% Create new column for pos_id

merge_11['pos_id'] = merge_11 ['Trader'].astype(str) + '20211111' + merge_11 ['Symbol']
merge_12['pos_id'] = merge_12 ['Trader'].astype(str) + '20211112' + merge_12 ['Symbol']

merge_11_12 = pd.concat([merge_11, merge_12])
print(merge_11_12)

# %% Aggregate quantity and notional for all trades by date, trader, symbol, ccy 

Grouped_trades = merge_11_12.groupby(['pos_id', 'cob_date', 'Trader', 'Symbol', 'Ccy',]).agg(Quantity = ('Quantity','sum'),Notional = ('Notional', 'sum')).reset_index()

Grouped_trades.info()

# %% Insert results into portfolio_positions

for i in range(len(Grouped_trades)):
    cur.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) \
                VALUES ("{Grouped_trades.loc[i,"pos_id"]}",\
                "{Grouped_trades.loc[i,"cob_date"]}",\
                "{Grouped_trades.loc[i,"Trader"]}",\
                "{Grouped_trades.loc[i,"Symbol"]}",\
                "{Grouped_trades.loc[i,"Ccy"]}",\
                {Grouped_trades.loc[i,"Quantity"]},\
                {Grouped_trades.loc[i,"Notional"]})')
                    
con_sql.commit()

#%% Close sql connection

con_sql.close()







