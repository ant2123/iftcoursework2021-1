#--------------------------------------------------------------------------------------
# Coursework2
# Author  : Hongyi Pang
# Create table
#--------------------------------------------------------------------------------------



Create_Table <- paste0("CREATE TABLE policy_breaches(
                      pos_id TEXT NOT NULL PRIMARY KEY,
                      date TEXT NOT NULL,
                      trader TEXT NOT NULL,
                      symbol TEXT NOT NULL,
                      ccy TEXT NOT NULL,
                      quantity INTEGER NOT NULL,
                      notional INTERGER NOT NULL,
                      mtm INTERGER NOT NULL,
                      limit_type TEXT NOT NULL,
                      limit_category TEXT NOT NULL,
                      FOREIGN KEY (symbol) REFERENCES equity_static (symbol)
                      )")



