///First query
db.CourseworkOne.find({$and:[{"StaticData.GICSSector":{"$eq":"Financials"}}, {"MarketData.Price":{"$lte":100}}]}).sort({"FinancialRatios.PERatio":1}).limit(10)

///Second query
db.CourseworkOne.aggregate([
  {$match:{}},
  {$group:{_id:"$StaticData.GICSSector",average_beta:{$avg:"$MarketData.Beta"}}},
  {$sort:{"average_beta":-1}}])