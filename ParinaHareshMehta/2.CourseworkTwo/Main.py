#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#--------------------------------------------------------------------------------------
# UCL -- Big Data in Quant Finance
# Author  : Parina Haresh Mehta
# Topic : Coursework Two
# Description : Main Python File
#--------------------------------------------------------------------------------------


# In[1]:


#Importing Mandatory Packages/Modules/Libraries
import pandas as pd
import pymongo
from IPython.display import display
import sqlite3 as sql
from sklearn import preprocessing
from kmodes.kprototypes import KPrototypes
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler


# In[2]:


client = pymongo.MongoClient("mongodb://localhost:27017/")
 
#Database Name
db = client["Equity"]
 
#Collection Name    
db.list_collection_names()


# In[15]:


import json
col = db["CourseworkTwo"]


# In[16]:


#Loading the MongoDB Database to Python
trade_collection = db.get_collection("CourseworkTwo")
with open("/Users/parinamehta/Desktop/iftcoursework2021/000.DataBases/NoSQL/CourseworkTwo.json") as f:
    file_data = json.load(f)


# In[17]:


trade_collection.insert_many(file_data)


# In[22]:


#Checking how many documents in JSON File
len(file_data)


# In[23]:


df = pd.DataFrame(list(col.find()))
df.head()
display (df)


# In[8]:


#Ensuring data is loaded correctly
file_data


# In[31]:


#Question 1.a to extract all trades on 2021-11-11
info =  col
filter1={
    'DateTime': {
    "$gte": 'ISODate(2021-11-11T00:00:00.000Z)',
    "$lt" :'ISODate(2021-11-11T23:59:59.000Z)'
    }
}
trades_date1 = info.find(filter=filter1)
trades_date1_df =  pd.DataFrame(list(trades_date1))
display(trades_date1_df)


# In[25]:


#Question 1.b to extract all trades on 2021-11-12
filter2={
    'DateTime': {
    "$gte": 'ISODate(2021-11-12T00:00:00.000Z)',
    "$lt" :'ISODate(2021-11-12T23:59:59.000Z)'
    }
}

trades_date2 = info.find(filter=filter2)
trades_date2_df3 =  pd.DataFrame(list(trades_date2))
display(trades_date2_df3)


# In[29]:


#Using K-means Clustering for 'Quantity' and 'Notional'
km = KMeans(n_clusters=2)
trade_predicted = km.fit_predict(trades_date2_df3[['Notional','Quantity']])

trades_date2_df3['cluster']=trade_predicted
trades_date2_df3.head()


trades_date2_df1 = trades_date2_df3[trades_date2_df3.cluster==0]

trades_date2_df2 = trades_date2_df3[trades_date2_df3.cluster==1]

trades_date2_df = trades_date2_df3.applymap(str)
display (trades_date2_df1)


# In[30]:


#The outlier in this cluster
display(trades_date2_df2)


# In[32]:


km = KMeans(n_clusters=2)
trade_predicted = km.fit_predict(trades_date1_df[['Notional','Quantity']])

trades_date1_df['cluster']=trade_predicted
trades_date1_df.head()


trades_date1_df1 = trades_date1_df[trades_date1_df.cluster==0]

trades_date1_df2 = trades_date1_df[trades_date1_df.cluster==1]
display (trades_date1_df1)


# In[33]:


#The outlier in another cluster
display(trades_date1_df2)


# In[ ]:


display(trades_date1_df2)
display(trades_date2_df2)


# In[34]:


#Creating a new table in SQL called trades_suspects
frames = [trades_date1_df2, trades_date2_df2]
trades_suspects = pd.concat(frames)
trades_suspects= suspect_trades.applymap(str)

conn = sql.connect('/Users/parinamehta/Desktop/iftcoursework2021/000.DataBases/SQL/Equity.db')
c = conn.cursor()
c.execute("""drop table if exists trades_suspects""")

create_trades_suspects_table= """CREATE TABLE "trades_suspects" (
    "_id"   TEXT,
    "DateTime"  TEXT,
    "TradeId"   TEXT,
    "Trader"    TEXT,
    "Symbol"    TEXT,
    "Quantity"  INTEGER,
    "Notional"  TEXT,
    "TradeType" TEXT,
    "Ccy"   TEXT,
    "Counterparty"  TEXT,
    "cluster"   INTEGER,
    PRIMARY KEY("_id")
);"""

c.execute(create_trades_suspects_table)
trades_suspects.to_sql('trades_suspects', conn, if_exists='append',index=False)


# In[36]:


df= df.applymap(str)
df.to_sql('all_trades', conn, if_exists='append',index=False)


# In[37]:


#aggregating Quantity and Notional for all trades by date, trader, symbol, ccy and insert the results into portfolio_positions
c.execute (""" INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)
select all_trades.tradeid||all_trades.DateTime||equity_static.symbol , 
all_trades.dateTime , all_trades.trader  , all_trades.symbol , all_trades.ccy , sum( quantity) , sum(notional)
from all_trades join equity_static 
on all_trades.symbol = equity_static.symbol
group by all_trades.tradeid||all_trades.dateTime||equity_static.symbol , 
all_trades.dateTime , all_trades.trader  , all_trades.symbol , all_trades.ccy
""")


# In[38]:


conn.commit()


# In[ ]:




