#Creating a new table in SQL called trades_suspects
frames = [trades_date1_df2, trades_date2_df2]
trades_suspects = pd.concat(frames)
trades_suspects= suspect_trades.applymap(str)

conn = sql.connect('/Users/parinamehta/Desktop/iftcoursework2021/000.DataBases/SQL/Equity.db')
c = conn.cursor()
c.execute("""drop table if exists trades_suspects""")

create_trades_suspects_table= """CREATE TABLE "trades_suspects" (
    "_id"   TEXT,
    "DateTime"  TEXT,
    "TradeId"   TEXT,
    "Trader"    TEXT,
    "Symbol"    TEXT,
    "Quantity"  INTEGER,
    "Notional"  TEXT,
    "TradeType" TEXT,
    "Ccy"   TEXT,
    "Counterparty"  TEXT,
    "cluster"   INTEGER,
    PRIMARY KEY("_id")
);"""

c.execute(create_trades_suspects_table)
trades_suspects.to_sql('trades_suspects', conn, if_exists='append',index=False)


# In[36]:


df= df.applymap(str)
df.to_sql('all_trades', conn, if_exists='append',index=False)