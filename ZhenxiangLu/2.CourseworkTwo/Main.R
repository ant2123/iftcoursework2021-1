#by zhenxiang(Francis)lu

#analysis date 2021-11-10.
#identify one fund/trader to monitor and check which benchmark could be the best to verify the performance.
#as starting point, construct a benchmark by using the MarketCap field provided in Coursework one.

library(lubridate)
library(dplyr)
library(RSQLite)        
library(dplyr)
library(mongolite) 
options(scipen = 999)
Args = c("C:/gitclone/iftcoursework2021/ZhenxiangLu/2.CourseworkTwo", "config/script.config", "config/script.params")
# Set working directory
setwd(Args[1])
# Source config, params
source(paste0( Args[2]))
source(paste0( Args[3]))
# Load R Scripts ----------------------------------------------------------
#-- Source all functions from files in ./modules/
invisible(lapply(list.files("./modules", recursive = T, full.names = T, pattern = ".R"), source))
# Set run date ------------------------------------------------------------
runDate <- as.Date("2021-11-10")
lagday <- runDate - 21 #one extra day to calculate return
oneyear <- runDate - 365
#-- create database connection
ConSql <- dbConnect(RSQLite::SQLite(), Config$SQLDataBase$Connection) # opens connection to SQL DB
#-- get data
positions <- getpositionSQL(ConSql)
prices <- getpriceSQL(ConSql)
traderinfo <- gettraderinfoSQL(ConSql)
# get market cap from CourseworkOne via MongoDB
conMongo <- mongo(collection = "CourseworkOne", db = "Equity", url = "mongodb://localhost",verbose = FALSE, options = ssl_options()) 
stockdata <- conMongo$find(query = "{ }", fields = "{}") 
CW2 <- mongo(collection = "Courseworktwo", db = "Equity", url = "mongodb://localhost",verbose = FALSE, options = ssl_options()) 
trades <- CW2$find(query = "{ }", fields = "{}") 

stockdata$MarketData$MarketCap <- as.numeric(stockdata$MarketData$MarketCap)
stockdata$MarketData$weight <- stockdata$MarketData$MarketCap/sum(as.numeric(stockdata$MarketData$MarketCap), na.rm = TRUE)
stockNA <- na.omit(stockdata)
benchmark <- data.frame(stockdata$Symbol,stockdata$MarketData$weight)
stockdata$MarketData$weight

#split by instrument 
listEquityPrices <- split(prices, prices$symbol_id)
#returns
listreturn <- lapply(listEquityPrices, function(x){
  x <- arrange(x, order(cob_date))
  x$LaggedPrices <- lag(x$close, 1)
  x$Return <- (x$close / lag(x$close, 1)) - 1
  return(x)
})
equityReturns <- as.data.frame(do.call(rbind, listreturn), stringsAsFactors = T)

#add weight
result <- left_join(equityReturns, benchmark, by = c("symbol_id"="stockdata.Symbol"))
result$weightedreturn <-result$Return*result$stockdata.MarketData.weight
splitday <- split(result, result$cob_date)

bmReturn <- lapply(splitday, function(x) sum(x$weightedreturn, na.rm = T))
bmReturn <- as.data.frame(do.call(rbind, bmReturn), stringsAsFactors = T)
colnames(bmReturn)
names(bmReturn)[1] <- "Benchmark Return"
bmReturn <- cbind(rownames(bmReturn), data.frame(bmReturn, row.names=NULL))
names(bmReturn)[1] <- "Date"
bmReturn$before <- as.numeric(as.Date(bmReturn$Date) - oneyear)
bmReturn <- bmReturn[which(bmReturn$before >= 0),]

#storing benchmark_returns
mydb <- dbConnect(RSQLite::SQLite(), "benchmark_returns.sqlite")
dbWriteTable(mydb, "benchmark_returns", bmReturn, overwrite=T)
#2021-11-10 over the past 20 days

#finally check, how many times the trader has exceeded the benchmark in the past 20 days prior to 2021-11-11.
#####
positions <- positions[which(positions$trader == trader),] #filter for the trader in question
positions$before <- as.numeric(as.Date(positions$cob_date) - lagday)# 20 days
positions <- positions[which(positions$before >= 0),]

mtm <- left_join(positions, prices, by = c("symbol"="symbol_id","cob_date" = "cob_date" ))
mtm$endofdayvalue <-mtm$net_quantity*mtm$close
splitbyday <- split(mtm, mtm$cob_date)
dailyposition <- lapply(splitbyday, function(x) sum(x$endofdayvalue, na.rm = T))
#
dailyposition <- as.data.frame(do.call(rbind, dailyposition), stringsAsFactors = T)
names(dailyposition)[1] <- "Net_Position"
dailyposition$lag <- lag(dailyposition$Net_Position, k =1)
dailyposition$return <- (dailyposition$lag-dailyposition$Net_Position)/dailyposition$Net_Position

dailyposition <- cbind(rownames(dailyposition), data.frame(dailyposition, row.names=NULL))
dailyposition <-na.omit(dailyposition)
names(dailyposition)[1] <- "Date"
names(dailyposition)[4] <- "Trader.Return"

result <- left_join(dailyposition, bmReturn, by = c("Date"="Date"))
result <-result[c("Date","Trader.Return","Benchmark.Return")]
result$outperform <- result$Trader.Return>result$Benchmark.Return
sum(result$outperform)
