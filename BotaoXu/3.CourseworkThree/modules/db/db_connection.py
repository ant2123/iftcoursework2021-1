import sqlite3
import pymongo

def sql_conn(db_name):
    conn=sqlite3.connect(db_name)
    c=conn.cursor()
    return conn,c

def mongo_conn(db_name, collection_name,url):
    client = pymongo.MongoClient(url)
    db = client[db_name]
    collection = db[collection_name]
    return collection