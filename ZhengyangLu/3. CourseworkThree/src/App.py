GITRepoDirectory = "D:/UCL/T1/Big data/Coursework 3/"


import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import datetime
import matplotlib.pyplot as plt
from pymongo import MongoClient
import configparser
from scipy.stats import norm
from scipy import stats

### Setting SQL connection ###

config_obj = configparser.ConfigParser()
config_obj.read(f"{GITRepoDirectory}/iftcoursework2021/ZhengyangLu/3. CourseworkThree/config/script.config")

year = config_obj.get('database1','courseyear')
data = config_obj.get('database1','data')
type = config_obj.get('database1','type')
database = config_obj.get('database1','database')

engine = create_engine(f"sqlite:///{GITRepoDirectory}/" +year+ data +type +database)
sqlcon = engine.connect()

### Setting MongoDB connection ###

config_obj.read(f"{GITRepoDirectory}/iftcoursework2021/ZhengyangLu/3. CourseworkThree/config/script.params")
host = config_obj.get('mongo','host')

mongocon = MongoClient(host)
db = mongocon.Equity
col = db.Coursework2
col2 = db.Coursework1

### Getting data for portfolio allocations ###

pipeline1 = [
    {'$match':{'Trader':'SML1458'}},
    {'$project':{
        'Trader':'$Trader',
        'Symbol':'$Symbol',
        'Quantity':'$Quantity',
        'TradeType':'$TradeType',
        'Currency':'$Ccy',
        'Date':'$DateTime'
           
    }}
]

df1 = pd.DataFrame()
for doc in col.aggregate(pipeline1): 
    df1 = df1.append(doc,ignore_index=True)

df2 = df1[df1['Date'].str.contains('2021-11-12')]
daytrades = pd.DataFrame(df2,columns = ['Trader','Symbol','Quantity','TradeType','Currency']).reset_index(drop = True)

x = {'Quantity': 'sum'}
allocation = daytrades.groupby('Symbol', as_index = False).aggregate(x).reindex(columns = daytrades.columns)
allocation = pd.DataFrame(allocation, columns = ['Symbol','Quantity']).sort_values('Symbol',ascending = True)
allocation = allocation[allocation.Quantity !=0].reset_index(drop = True)

sumq = allocation['Quantity'].sum()
allocation['Weights'] = 0
allocation['Weights'] += allocation['Quantity']/sumq
allocation['Weights'] = allocation['Weights'].apply(pd.to_numeric,errors = 'coerce')

### Getting price data ###

def SQLfinprice():
    return("SELECT equity_static.symbol, equity_static.GICSSector,equity_prices.cob_date, equity_prices.close FROM equity_static join equity_prices on equity_static.symbol= equity_prices.symbol_id and equity_static.GICSSector = 'Financials'")

df3 = pd.read_sql_query(SQLfinprice(),engine)
finprice = pd.DataFrame(df3, columns = ['symbol','GICSSector','cob_date','close'])
date_strings = finprice["cob_date"].to_list()
date = []

for i in range(len(date_strings)):
    date += [datetime.datetime.strptime(date_strings[i], "%d-%b-%Y")]
finprice['datetime']= date
finprice.rename(columns = {'symbol':'Symbol','close':'Close','datetime':'Date'},inplace = True)

finprice = finprice.drop(['cob_date','GICSSector'],1)
finprice = finprice[finprice.Symbol !='ETFC']
finprice = finprice.groupby(['Date','Symbol'])['Close'].first().unstack()

symbol = allocation['Symbol'].tolist()
df4 = finprice.copy()
portprice = df4[symbol]

portprice_mat = np.array(portprice)
weights_vec = np.array(allocation['Weights'])
portprice_his = portprice_mat@weights_vec

port_trends = portprice.copy()
port_trends['PortfolioPrice'] = 0
port_trends['PortfolioPrice'] += portprice_his
port_trends = port_trends.drop(symbol,1)

portreturn = portprice.pct_change(1)
portreturn = portreturn.fillna(0)

portreturn_mat = np.array(portreturn)
porthis = portreturn_mat@weights_vec

portreturn['DailyReturns'] = 0
portreturn['DailyReturns'] += porthis
portreturn['CumulativeReturns'] = 0
portreturn['CumulativeReturns'] += (portreturn['DailyReturns']+1).cumprod()
port_historical = portreturn.drop(symbol,1)

### Creating Benchmark ###



pipeline2 = [
    {'$match':{'StaticData.GICSSector':'Financials'}},
    {"$unwind":{'path': '$MarketData', 'preserveNullAndEmptyArrays': True}},
    {"$unwind":{'path': '$MarketData.MarketCap', 'preserveNullAndEmptyArrays': True}},
    {'$project':{
      'Symbol':'$Symbol',
      'MarketCap':"$MarketData.MarketCap" 
    }}
]

df5 = pd.DataFrame()
for doc in col2.aggregate(pipeline2): 
    df5 = df5.append(doc,ignore_index=True)

fincap = pd.DataFrame(df5, columns = ['Symbol','MarketCap'])
fincap.dropna(subset=['MarketCap'], inplace = True)

fincap = fincap[fincap.Symbol !='BBT']
fincap = fincap[fincap.Symbol !='L']
fincap = fincap[fincap.Symbol !='LNC']
fincap = fincap[fincap.Symbol !='PFG']
fincap = fincap[fincap.Symbol !='STI']
fincap = fincap[fincap.Symbol !='ZION']
fincap = fincap[fincap.Symbol !='ETFC']
fincap = fincap.reset_index(drop = True)

sumcap = fincap['MarketCap'].sum()
fincap['Weights'] = 0
fincap['Weights'] += fincap['MarketCap']/sumcap
fincap = fincap.sort_values('Symbol',ascending = True)

finprice_mat=np.array(finprice)
bench_vec = np.array(fincap['Weights'])
benchprice = finprice_mat@bench_vec

symbol2 = fincap['Symbol']
benchmark = finprice.copy()
benchmark['Benchmark'] = 0
benchmark['Benchmark'] += benchprice
benchmark = benchmark.drop(symbol2,1)

benchmark['DailyReturns'] = 0
benchmark['DailyReturns'] += benchmark['Benchmark'].pct_change(1).fillna(0)
benchmark['CumulativeReturns'] = 0
benchmark['CumulativeReturns'] += (benchmark['DailyReturns']+1).cumprod()

### Essential figures for HTML report ###

plt.figure(figsize = (16,8))
plt.plot(benchmark['CumulativeReturns'],label = 'Benchmark 2020/01/02 - 2021/11/12')
plt.plot(port_historical['CumulativeReturns'],label = 'Portfolio 2020/01/02 - 2021/11/12',color = 'red')
plt.title('Cumulative Returns Comparision',size = 18)
plt.ylabel('Cumulative Returns', size = 14)
plt.legend()
plt.savefig(f"{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/comparision.png")

ma = port_historical.copy()
ma['MA50'] = ma['CumulativeReturns'].rolling(50).mean()
ma['MA160'] = ma['CumulativeReturns'].rolling(160).mean()
plt.figure(figsize = (16,8))
plt.plot(ma['CumulativeReturns'],color='red', label = 'Portfolio')
plt.plot(ma['MA50'],color='m', label = 'Portfolio 50-Days moving average')
plt.plot(ma['MA160'],color='c', label = 'Portfolio 160-Days moving average')
plt.title('Portfolio Moving Average',size = 18)
plt.ylabel('Cumulative Returns', size =14)
plt.legend()
plt.savefig(f"{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/moving.png")

port_mean = port_historical.copy()
port_mean['Mean'] = port_mean['DailyReturns'].mean()
plt.figure(figsize = (16,8))
plt.plot(port_historical['DailyReturns'],label = '2020/01/02 - 2021/11/12',color = 'coral')
plt.plot(port_mean['Mean'],label = 'Mean Daily Return: 0.11%',color = 'royalblue',linestyle = '--')
plt.title('Portfolio Historical Returns',size = 18)
plt.ylabel('Daily Returns', size = 14)
plt.legend()
plt.savefig(f"{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/returns.png")

plt.figure(figsize = (16,8))
plt.hist(port_historical['DailyReturns'],label = 'Portfolio',color ='red',bins = 120, alpha = 0.5)
plt.hist(benchmark['DailyReturns'],label = 'Benchmark',color = 'b',bins = 120, alpha = 0.5)
plt.title('Returns Distribution Comparison',size = 18)
plt.ylabel('Frequency',size=14)
plt.legend()
plt.savefig(f"{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/distribution.png")

top5 = allocation.copy()
top5 = top5.sort_values('Weights',ascending = False)


top5_2 = top5[:5].copy()
new_row = pd.DataFrame(data = {
    'Symbol' : ['Others'],
    'Weights' : [top5['Weights'][5:].sum()]
})
top5_final = pd.concat([top5_2, new_row])
plt.figure(figsize = (16,8))
my_explode = (0,0,0,0,0,0.1)
my_colors = ['orangered','lime','yellow','aqua','royalblue','silver']
plt.pie(top5_final['Weights'],labels = top5_final['Symbol'],autopct = '%1.1f%%',colors = my_colors, explode = my_explode, shadow = True,startangle = 80)
plt.title('Top 5 Allocations in Portfolio',size = 18)
plt.legend()
plt.savefig(f"{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/top5.png")

### Mean returns ###

mean_ben = benchmark['DailyReturns'].mean()
mean_port = port_historical['DailyReturns'].mean()

### Total returns ###

total_ben = benchmark['CumulativeReturns'].iloc[-1]-1
total_port = port_historical['CumulativeReturns'].iloc[-1]-1

### Annualized returns ###

annual_re_ben = (total_ben+1)**(365/472)-1
annual_re_port = (total_port+1)**(365/472)-1

### Standard deviations ###

daily_sd_ben = benchmark['DailyReturns'].std()
daily_sd_port = port_historical['DailyReturns'].std()
annual_sd_ben = daily_sd_ben * np.sqrt(253)
annual_sd_port = daily_sd_port * np.sqrt(253)

### Sharpe ratio ###

rf = 0.0178
sharpe_ben = (annual_re_ben-rf)/annual_sd_ben
sharpe_port = (annual_re_port-rf)/annual_sd_port

### Sortino ratio ###

std_neg_ben = benchmark.DailyReturns[benchmark['DailyReturns']<0].std()*np.sqrt(253)
std_neg_port = port_historical.DailyReturns[port_historical['DailyReturns']<0].std()*np.sqrt(253)
sortino_ben = (annual_re_ben-rf)/std_neg_ben
sortino_port = (annual_re_port-rf)/std_neg_port

### Beta ###

(beta,alpha)= stats.linregress(port_trends['PortfolioPrice'],
                benchmark['Benchmark'])[0:2]



beta_port = round(beta,2)
beta_ben = 1.000

### Treynor Ratio ###

tre_ben = (annual_re_ben-rf)/beta_ben
tre_port = (annual_re_port-rf)/beta_port

### Max drawdown ###

max_min_ben = np.ptp(benchmark['CumulativeReturns'])
max_min_port = np.ptp(port_historical['CumulativeReturns'])
max_draw_ben = max_min_ben/benchmark['CumulativeReturns'].max()
max_draw_port = max_min_port/port_historical['CumulativeReturns'].max()

### VaR ###

confidence_level = .99
z_score_cut_off = norm.ppf(1-confidence_level, 0, 1)
VaR_ben = z_score_cut_off * annual_sd_ben 
VaR_port = z_score_cut_off * annual_sd_port

### Calmar ratio ###

calmar_ben = (annual_re_ben-rf)/max_draw_ben
calmar_port = (annual_re_port-rf)/max_draw_port

### Worst day ###

worst_ben = benchmark['DailyReturns'].min()
worst_port = port_historical['DailyReturns'].min()

### Best day ###

best_ben = benchmark['DailyReturns'].max()
best_port = port_historical['DailyReturns'].max()

### Porformance table ###

performance_data = {'Portfolio':[total_port,annual_re_port,mean_port,rf,beta_port,sharpe_port,sortino_port,tre_port],'Benchmark':[total_ben,annual_re_ben,mean_ben,rf,beta_ben,sharpe_ben,sortino_ben,tre_ben]}
performance_table = pd.DataFrame(performance_data,index=['Total Return','Anuualized Return','Average Daily Return','Risk Free Rate','Beta','Sharpe Ratio','Sortino Ratio','Treynor Ratio']).round(3)

### Risk table ###

risk_data = {'Portfolio':[daily_sd_port,annual_sd_port,max_min_port,max_draw_port,calmar_port,VaR_port,best_port,worst_port],'Benchmark':[daily_sd_ben,annual_sd_ben,max_min_ben,max_draw_ben,calmar_ben,VaR_ben,best_ben,worst_ben]}
risk_table = pd.DataFrame(risk_data,index=['Daily Volatility','Anuualized Volatility','Cumulative Return Range','Max Drawdown','Calmar Ratio','VaR','Best Day','Worst Day']).round(3)

### Creating HTML report ###

html = f'''
<html>
    
        <head>
            <title>Simon Mare Report</title>
        </head>
        
        <body>
            
   
    <h1>Portfolio Performance Report</h1>
    <h5>Trader: Simon Mare | ID: SML1458 | Date:12-11-2021 | Fund: US S&P500 Financials </h5>
    <h5>Period: 02-01-2020 to 12-11-2021</h5>
<hr>
       <table><tr>
       <td><img src='{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/comparision.png' width="1000">
       <td><h3>Performance Metrics Comparison</h3>
           {performance_table.to_html()}
        </tr></table>
        
         
         <table><tr>
         <td><img src='{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/moving.png' width="1000">
         <td><h3>Risk Metrics Comparison</h3>
             {risk_table.to_html()}
        </tr></table>
        
        <table><tr>
        <img src='{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/returns.png' width="1500">
        <img src='{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/distribution.png' width="1500">
        <img src='{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/modules/output/top5.png' width="1500">
        
    

        </body>
    </html>

'''

with open(f"{GITRepoDirectory}iftcoursework2021/ZhengyangLu/3. CourseworkThree/Home.html", 'w') as f: 
    f.write(html)


### End ###
sqlcon.close()
mongocon.close()
