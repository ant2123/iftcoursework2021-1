/*
UCL -- Big Data in Quantitative Finance

Author : Keisi Mancellari
Topic  : Coursework 1 - SQL
*/

-- Selecting relevant columns 
SELECT price_id, open, low, close, volume, cob_date, symbol_id FROM equity_prices WHERE cob_date = '02-Jan-2020';
SELECT price_id, open, low, close, volume, cob_date, symbol_id FROM equity_prices WHERE volume > 1000000;
SELECT price_id, open, low, close, volume, cob_date, symbol_id FROM equity_prices WHERE close > 500;

SELECT * FROM equity_static WHERE GICSSector = 'Industrials';
SELECT * FROM equity_static WHERE GICSIndustry LIKE '%Finance%'; 		
SELECT symbol FROM equity_static WHERE GICSSector IN ('Industrials','Materials');

SELECT DISTINCT trader_id FROM trader_limits;
SELECT DISTINCT cob_date FROM equity_prices;
SELECT DISTINCT GICSSector FROM equity_static;
SELECT * FROM trader_limits WHERE limit_end IS NOT NULL;
SELECT * FROM trader_limits WHERE currency = 'USD';

-- Grouping by different types of categories
SELECT * FROM trader_limits GROUP BY limit_type;
SELECT * FROM trader_limits GROUP BY limit_category;
SELECT * FROM trader_limits GROUP BY trader_id;

SELECT limit_type, COUNT(limit_id) AS no_limits FROM trader_limits GROUP BY limit_type;
SELECT limit_category, COUNT(limit_id) AS no_limits FROM trader_limits GROUP BY limit_category;
SELECT trader_id, COUNT(limit_id) AS no_limits FROM trader_limits GROUP BY trader_id;
SELECT GICSIndustry, COUNT(security) AS no_securities FROM equity_static GROUP BY GICSSector;

-- Calculating sum, mean, min, max and grouping by
SELECT cob_date,
SUM(volume) AS sum_volume,
ROUND(AVG(volume), 1) AS mean_volume, 
MIN(volume), MAX(volume) 
FROM equity_prices
WHERE close > 80
GROUP BY cob_date
ORDER BY sum_volume DESC
LIMIT 10;

SELECT cob_date,
ROUND(AVG(open), 1) AS mean_open,
ROUND(AVG(high), 1) AS mean_high,
ROUND(AVG(low), 1) AS mean_low,
ROUND(AVG(close), 1) AS mean_close
FROM equity_prices
WHERE volume > 1000000
GROUP BY cob_date
ORDER BY mean_high DESC
LIMIT 10;

-- Joining tables
SELECT * FROM equity_static
LEFT JOIN equity_prices ON equity_static.symbol = equity_prices.symbol_id
WHERE symbol_id IS NOT NULL;

SELECT AVG(open) AS avg_open, AVG(close) AS avg_close, AVG(volume) AS avg_vol, symbol_id FROM equity_prices
LEFT JOIN equity_static ON equity_prices.symbol_id = equity_static.symbol
GROUP BY symbol_id
HAVING avg_close > 100
ORDER BY avg_close DESC;

SELECT portfolio_positions.trader, portfolio_positions.cob_date, portfolio_positions.symbol, portfolio_positions.net_quantity, portfolio_positions.net_amount, 
portfolio_positions.net_quantity * equity_prices.open AS open_amount,
portfolio_positions.net_quantity * equity_prices.low AS low_amount,
portfolio_positions.net_quantity * equity_prices.high AS high_amount,
portfolio_positions.net_quantity * equity_prices.close AS close_amount
FROM portfolio_positions
LEFT JOIN equity_prices ON portfolio_positions.symbol = equity_prices.symbol_id AND portfolio_positions.cob_date = equity_prices.cob_date;


