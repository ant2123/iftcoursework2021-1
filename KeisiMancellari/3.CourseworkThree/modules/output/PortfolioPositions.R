#--------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Keisi Mancellari
# Topic   : Coursework 3
# File    : PortfolioPositions.R
#--------------------------------------------------------------------------

library(dplyr)

# Query to aggregate trades

traderPosition <- allTrades %>% 
  group_by(., DateTime, Trader, Symbol, Ccy) %>% 
  summarise(., net_quantity = sum(Quantity), net_amount = sum(Notional))

names(traderPosition)[1:4] <- c("cob_date", "trader", "symbol", "ccy")

traderPosition$pos_id <- paste0(traderPosition$trader, traderPosition$cob_date, traderPosition$symbol)

# Choose trader DGR1983 to monitor position

oneTrader <- traderPosition %>% filter(trader == "DGR1983")
traderOne <- as.data.frame(oneTrader)



