#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Yufan Xie
# Topic   : Import and clean up the data 
#--------------------------------------------------------------------------------------

# Select required data from data frame
# Data from 11-11 to 11-12
Data_1112 <- conMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-11T00:00:00.000Z)\", \"$lt\": \"ISODate(2021-11-13T00:00:00.000Z)\"}}")
# Change DateTime to Date
Data_1112$DateTime <- as.Date(substr(Data_1112$DateTime,9,18))
# Change Column name of DateTime into Date
names(Data_1112)[names(Data_1112) == 'DateTime'] <- 'Date' 
# aggregate all trades by Trader, ISIN, Currency and Date
Data_1112_A <- aggregate(Data_1112[c("Quantity","Notional")],by=Data_1112[c("Trader","Symbol","Ccy","Date")],
                         FUN=sum)


# Delete irrelevant trade limit policy
TraderLimits_Clean <- TraderLimits[is.na(TraderLimits["limit_end"]),]

# Turning Value for PCG into percentage
for(i in 1:nrow(TraderLimits_Clean)) {
  if (TraderLimits_Clean$limit_category[i] =="relative"){
    TraderLimits_Clean$limit_amount[i] <- TraderLimits_Clean$limit_amount[i]/100
  }
}
