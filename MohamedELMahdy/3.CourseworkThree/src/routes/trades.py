from fastapi import APIRouter
from fastapi import HTTPException, status
from fastapi.responses import JSONResponse
from typing import Optional

import modules.db.mongo as mongodb
from modules.trades import Item

router = APIRouter()


@router.post("/trade/", response_description="API_Trades_Input")
async def create_item(trade: Item):
    trade_handshake = await mongodb.load_trade(trade)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=trade_handshake)
