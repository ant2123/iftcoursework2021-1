#--------------------------------------------------------------------------------------
#Coursework2
# Author  : Yin Duan
# Topic   : Create Table
#--------------------------------------------------------------------------------------

Create_Table <- paste0("CREATE TABLE policy_breaches(
                      pos_id TEXT NOT NULL PRIMARY KEY,
                      trader TEXT NOT NULL,
                      symbol TEXT NOT NULL,
                      ccy TEXT NOT NULL,
                      date TEXT NOT NULL,
                      quantity INTEGER NOT NULL,
                      notional INTERGER NOT NULL,
                      amount INTERGER NOT NULL,
                      GICSSector TEXT NOT NULL,
                      limit_type TEXT NOT NULL,
                      FOREIGN KEY (symbol) REFERENCES equity_static (symbol)
                      )")




