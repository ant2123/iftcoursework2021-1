#--------------------------------------------------------------------------------------
# 
# Check that trades are consistent with expectations 
#
#--------------------------------------------------------------------------------------

# Test Notional against Quantity -------------------------------------------

testEquityPrice <- function(EquityPriceDf, symbolAverage){
  symbolBucket <- unique(EquityPriceDf$Symbol)
  idSymbol <- which(names(symbolAverage) == as.character(symbolBucket))
  
  thisSymbolMean <- symbolAverage[idSymbol]
  
  testResults <- abs(abs(EquityPriceDf$AveragePrice / thisSymbolMean) - 1)
  
  RetainEquity <- testResults < 0.15
  output <- as.data.frame(list(ID = EquityPriceDf$TradeId,
                               Date = EquityPriceDf$DateTime,
                               Trader = EquityPriceDf$Trader,
                               Symbol = symbolBucket,
                               Retain = RetainEquity,
                               Quantity = EquityPriceDf$Quantity,
                               Notional = EquityPriceDf$Notional,
                               TradeType = EquityPriceDf$TradeType,
                               Ccy = EquityPriceDf$Ccy,
                               SymbolMean = thisSymbolMean,
                               PriceMean = mean(EquityPriceDf$AveragePrice, na.rm = T)),
                          stringsAsFactors = F)
  return(output)
}

