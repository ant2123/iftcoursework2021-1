# ---------------------------------------------------------------------------------
#
# Get portfolio positions and equity prices
#
# ---------------------------------------------------------------------------------


# Get Portfolio Positions ---------------------------------------------------------

getPortfolioPositionsSQL <- function(conSql){
  portfolioPositions <- RSQLite::dbGetQuery(conSql, paste0("SELECT cob_date, trader, symbol, ccy, net_quantity, net_amount FROM portfolio_positions"))
  portfolioPositions$cob_date <- lubridate::dmy(portfolioPositions$cob_date)
  return(portfolioPositions)
}


# Get Equity Prices -----------------------------------------------------------------

getEquityPriceSQL <- function(conSql){
  SQLQueryPrices <- paste0("SELECT max(high) AS high , min(low) AS low, volume, currency, cob_date, symbol_id FROM equity_prices 
                            WHERE cob_date = '11-Nov-2021' OR cob_date = '12-Nov-2021'
                            GROUP BY symbol_id")
  equityPrices <- RSQLite::dbGetQuery(conSql, SQLQueryPrices)
  equityPrices$cob_date <- lubridate::dmy(equityPrices$cob_date)
  return(equityPrices)
}




