#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Coursework Two : 2022-01-31
# Topic   : Run HTML
#--------------------------------------------------------------------------------------

# Run HTML------------------------------------------------------------------------
setwd("./modules/StepThree")
render("report.Rmd", output_file = "../../Home.html")
setwd("../../")
browseURL("./Home.html")
