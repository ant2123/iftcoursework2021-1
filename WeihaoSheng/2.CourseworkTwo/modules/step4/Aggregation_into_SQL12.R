#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Coursework Two : 2021-12-19
# Topic   : Aggregation12
#--------------------------------------------------------------------------------------

# Create a new column with value 12-Nov-2021 in table Day12
Sys.setlocale("LC_TIME", "English")
Day12$Date <- format(as.Date("2021-11-12"),"%d-%b-%Y")
Day12

# aggregate quantity on day 12 based on date, trader, symbol, Ccy-----------------------------
agger_quantityDay12 <- aggregate(Quantity~Date+Trader+Symbol+Ccy,Day12,sum)
aggre_amountsDay12 <- aggregate(Notional~Date+Trader+Symbol+Ccy,Day12,sum)

# rename the last column
names(agger_quantityDay12)[names(agger_quantityDay12) == "Quantity"] <- "net_quantity"
names(aggre_amountsDay12)[names(aggre_amountsDay12) == "Notional"] <- "net_amount"

# Join two table together
Day12Join <- left_join(agger_quantityDay12,aggre_amountsDay12)

# Primary Key
Day12Join$Key <- paste0(Day12Join$Trader,20211112, Day12Join$Symbol)

# Insert in SQL DB
for(i in 1:nrow(Day12Join)){
  
  dbExecute(conSql, paste0("INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) ",
                           "VALUES (\"",
                           as.character(Day12Join$Key[i]),"\",",
                           "\"",as.character(Day12Join$Date[i]),"\",",
                           "\"",as.character(Day12Join$Trader[i]),"\",",
                           "\"",as.character(Day12Join$Symbol[i]),"\",",
                           "\"",as.character(Day12Join$Ccy[i]),"\",",
                           Day12Join$net_quantity[i],",",
                           Day12Join$net_amount[i],
                           ")"))}

printInfoLog("AggregationDay12.....complete!")
