import json
from pymongo import MongoClient

def load_mongo():
    client = MongoClient('localhost', 27017)
    db = client['CourseworkTwo']
    if 'CourseworkTwo' in client.database_names():
        return

    collection_trade = db['nonsql']
    with open('./modules/db/CourseworkTwo.json') as f:
        file_data = json.load(f)

    collection_trade.insert_many(file_data)