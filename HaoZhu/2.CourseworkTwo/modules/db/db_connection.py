import sqlite3
import pymongo


def sql_connect(db_name):
    connection = sqlite3.connect(db_name)
    cursor = connection.cursor()
    return connection, cursor


def mongo_connect(db_name, collection_name, url):
    client = pymongo.MongoClient(url)
    db = client[db_name]
    collection = db[collection_name]
    return collection
