from re import S
from typing import List, Optional, Union
import pymongo

from static.DataItem import TradeItem


class MongoDB:
    '''
    Provide a unified object to operate the database, which packages the methods commonly used in this project.
    '''
    __host = 'localhost'
    __port = 27017

    __db_client = None

    __current_database = None
    __current_collection = None

    def __init__(self, host: str, port:  Optional[int], database: str, collection: Optional[str]) -> None:
        '''
        Initialize the conection to target database.
        The database name is required. Bind the collection is allowed at the same time.
        '''
        self.__host = host
        self.__port = port
        self.__db_client = pymongo.MongoClient(
            host=self.__host, port=self.__port)

        self.__current_database = self.__db_client.get_database(database)
        if collection is not None:
            self.__current_collection = self.__current_database.get_collection(
                collection)

    def change_database(self, database: str) -> None:
        '''
        Change current database.
        '''
        self.__current_database = self.__db_client.get_database(database)
        print('Use ' + database)

    def change_collection(self, collection: str) -> None:
        '''
        Change current collection.
        '''
        self.__current_collection = self.__current_database.get_collection(
            collection)
        print('Use ' + str(self.__current_database.name) + '.'+collection)

    def add_data(self, data: TradeItem) -> None:
        '''
        Add data to the current collection.
        '''
        temp = {"DateTime": data.DateTime, "TradeId": data.TradeId, "Trader": data.Trader, "Symbol": data.Symbol, "Quantity": data.Quantity,
                "Notional": data.Notional, "TradeType": data.TradeType, "Ccy": data.Ccy, "Counterparty": data.Counterparty}
        self.__current_collection.insert_one(temp)

    def del_one(self, key: dict) -> None:
        '''
        Remove specified data.
        '''
        self.__current_collection.delete_one(key)

    def del_many(self, key: dict) -> None:
        '''
        Remove specified data.
        '''
        self.__current_collection.delete_many(key)

    def find_data(self, key: Optional[dict], filter: Optional[dict]) -> List[dict]:
        '''
        Filter data. 
        '''
        result = []
        if filter == None:
            if key == {}:
                result = [x for x in self.__current_collection.find()]
            else:
                result = [x for x in self.__current_collection.find(key)]
        else:
            result = [x for x in self.__current_collection.find(key, filter)]

        return result

    def close(self) -> None:
        '''
        Close the connection.
        '''
        self.__db_client.close()
