import json
from pymongo import MongoClient

def load_mongo():
    client = MongoClient('localhost', 27017)
    db = client.CourseworkTwo
    if 'CourseworkTwo' in client.list_database_names():
        return

    collection_trade = db['nonsql']
    with open('./000.DataBases/NoSQL/CourseworkTwo.json') as f:
        file_data = json.load(f)

    collection_trade.insert_many(file_data)