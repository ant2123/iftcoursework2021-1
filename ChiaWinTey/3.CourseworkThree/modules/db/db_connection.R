#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chia Win Tey
# Topic   : Coursework Three (Case 3A)
# File    :db_connection.R
#--------------------------------------------------------------------------------------


# 1. SQLite Query to load Static Bond ----------------------------------------

 Equitystatic <- paste0("SELECT * FROM equity_static")
 
 Portfolio <- paste0("SELECT * FROM portfolio_positions")

 Closeprice <- paste0("SELECT close,cob_date,symbol_id FROM equity_prices")
 
