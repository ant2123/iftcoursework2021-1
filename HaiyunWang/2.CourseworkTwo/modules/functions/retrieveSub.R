# retrieveSub function

# Import all values in R
fullTrades <- conMongo$find(query = "{}")
head(fullTrades)

# Retrieve all trades on 2021-11-11 & 2021-11-12
library(stringr)
library(dplyr)
data_sub = fullTrades%>%mutate(DateTime = str_sub(DateTime, start = 9, end = 18))%>%
  filter(DateTime%in%c("2021-11-11","2021-11-12"))
trades11 = fullTrades%>%mutate(DateTime = str_sub(DateTime, start = 9, end = 18))%>%
  filter(DateTime%in%"2021-11-11")
trades12 = fullTrades%>%mutate(DateTime = str_sub(DateTime, start = 9, end = 18))%>%
  filter(DateTime%in%"2021-11-12")
head(data_sub)
head(trades11)
head(trades12)