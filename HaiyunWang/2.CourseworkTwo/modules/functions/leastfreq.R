# Extract the least frequent categorical value
calculate_mode <- function(x) {
  uniqx <- unique(na.omit(x))
  uniqx[which.min(tabulate(match(x, uniqx)))]
}
