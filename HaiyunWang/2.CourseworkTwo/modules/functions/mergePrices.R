# mergePrices function

# Add implied, high, low & close prices into the dataset
# implied price = Notional / Quantity

# 2021-11-11
# Extract high, low & close prices
price11 <- equityPrices[equityPrices$cob_date=="2021-11-11", ]
closePrices11 <- price11[c("symbol_id", "close")]
names(closePrices11)[names(closePrices11) == 'symbol_id'] <- 'Symbol'
highPrices11 <- price11[c("symbol_id", "high")]
names(highPrices11)[names(highPrices11) == 'symbol_id'] <- 'Symbol'
lowPrices11 <- price11[c("symbol_id", "low")]
names(lowPrices11)[names(lowPrices11) == 'symbol_id'] <- 'Symbol'
# Merge the above real market prices into the dataset
mergePrice11 <- merge(c(highPrices11, lowPrices11, closePrices11),trades11[c("Quantity", "Notional", "Symbol", "TradeId")],by="Symbol", no.dups = TRUE)
mergePrice11 <- subset(mergePrice11, select = c(-3, -5))
# Calculate and insert the implied price
mergePrice11$impliedPrice <- mergePrice11$Notional / mergePrice11$Quantity

# 2021-11-12
# Extract high, low & close prices
price12 <- equityPrices[equityPrices$cob_date=="2021-11-12", ]
closePrices12 <- price12[c("symbol_id", "close")]
names(closePrices12)[names(closePrices12) == 'symbol_id'] <- 'Symbol'
highPrices12 <- price12[c("symbol_id", "high")]
names(highPrices12)[names(highPrices12) == 'symbol_id'] <- 'Symbol'
lowPrices12 <- price12[c("symbol_id", "low")]
names(lowPrices12)[names(lowPrices12) == 'symbol_id'] <- 'Symbol'
# Merge the above real market prices into the dataset
mergePrice12 <- merge(c(highPrices12, lowPrices12, closePrices12),trades12[c("Quantity", "Notional", "Symbol", "TradeId")],by="Symbol", no.dups = TRUE)
mergePrice12 <- subset(mergePrice12, select = c(-3, -5))
# Calculate and insert the implied price
mergePrice12$impliedPrice <- mergePrice12$Notional / mergePrice12$Quantity