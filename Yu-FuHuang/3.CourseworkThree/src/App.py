from fastapi import FastAPI
from pydantic import BaseModel
import motor.motor_asyncio

app = FastAPI()

client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://localhost:27017')
database = client.Equity
collection = database.CourseworkTwo

class Trade(BaseModel):
    DataTime: str
    TradeId: str
    Trader: str
    Symbol: str
    Quantity: int
    Notional: float
    TradeType: str
    Ccy: str
    Counterparty: str

async def create_trade(trade):
    document = trade
    result = await collection.insert_one(document)
    return document

@app.post("/api/trade", response_model=Trade)
async def post_trade(trade:Trade):
    response = await create_trade(trade.dict())
    if response:
        return response