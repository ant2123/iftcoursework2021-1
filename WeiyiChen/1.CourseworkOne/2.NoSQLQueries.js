##Query One:Find data whose beta are between 0.5 and 2, 
and see top ten companies and their industry categories based on PERatio

db.CourseworkOne.find(
{"MarketData.Beta": {"$gte" : 0.5, "$lte":1.5}},
{"StaticData.Security":1,"MarketData.Price":1, "StaticData.GICSSector":1}
).sort({"FinancialRatios.PERatio":-1}).limit(10).pretty()

##Query Two:Calculate the average PE ratio and count the number of companies 
in different industries whose PE Ratio are between 20 and 30

db.CourseworkOne.aggregate([
{$match:{"FinancialRatios.PERatio":{"$gte":20,"$lte":30}}},
{$group:{_id:"$StaticData.GICSSector", average:{$avg:"$FinancialRatios.PERatio"} ,count:{$sum:1} }}
])