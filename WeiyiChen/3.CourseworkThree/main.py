#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Weiyi Chen
# Coursework : Coursework3
# Use Case   : Case Three.b
# File       : Main.py
#--------------------------------------------------------------------------------------

##import libraries
from pymongo import MongoClient
import pandas as pd
import sqlite3 
from bson.json_util import dumps
from sqlalchemy import *
import numpy as np
from datetime import datetime
from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel

##choose fastapi
app = FastAPI()
##sqlite connection
GITRepoDirectory = "/Users/chenweiyi/Desktop/python/bigdata"
engine_1 = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine_1.connect()
equity_prices = pd.read_sql_table("equity_prices", engine_1)

##define item
class Item(BaseModel):
    DateTime: str
    TradeId: str
    Trader:str
    Quantity:str
    Notional:str
    TradeType:str
    Ccy:str
    Counterparty:str
    Symbol:str

#sanity check
def sanity_check(new_trades:Item):
    global engine_1
    global con
    global equity_prices

    ##find transaction date
    dt_new = new_trades.DateTime
    dt_new = datetime.strptime(dt_new, '%Y-%m-%d')
    day = dt_new.strftime("%d-%b-%Y")
    equity_prices = equity_prices[equity_prices['cob_date']==day] 

    #find matching transaction record
    import json
    price =  float(new_trades.Notional)/float(new_trades.Quantity) 
    new_trades = new_trades.__dict__
    equity = equity_prices[equity_prices.symbol_id == new_trades['Symbol']]
    
    # find suspect data 
    suspect = []
    print('price : ',price)
    print('equity.high: ',equity.high.iloc[0])
    print('equity.low: ',equity.low.iloc[0])
    if price>equity['high'].iloc[0] or price< equity['low'].iloc[0]:
        suspect.append(new_trades)
    if suspect == []:
        print('sanity check passed')
        return True
    else:
        print('suspect :  ', suspect)
        print('sanity check failed')
        return False
   
## written into MongoDB Database Equity Collection CourseworkTwo
def write_db(item:Item):
    ##mongodb connection
    con = MongoClient('mongodb://localhost')
    db = con.EquitySP500
    col = db.CourseworkTwo
    cw = pd.DataFrame(list(col.find({})))
    ##export to mongodb
    item = item.__dict__
    print('item: ',item)
    db.col.insert_one(item)


##create a single end point api where traders can submit trades
@app.put("/trades")
def update_item(item: Item):
    #print(item)
    if sanity_check(item):
        write_db(item)
        
        return {"DateTime":item.DateTime, "TradeId":item.TradeId, "Trader":item.Trader, "Quantity":item.Quantity, "Notional":item.Notional, "TradeType":item.TradeType,"Ccy":item.Ccy,"Counterparty":item.Counterparty}
    else:
        return {'trade status': 'error'}

