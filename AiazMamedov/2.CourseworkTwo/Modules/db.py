from sqlalchemy import create_engine, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
import os

basedir = os.path.dirname(__file__)
path = os.path.join(basedir, 'Equity.db')
engine = create_engine('sqlite:///' + path)
db_session = scoped_session(sessionmaker(bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


class NewTable(Base):
    __tablename__ = 'trades_suspects'
    id = Column(Integer, primary_key=True)
    date = Column(String)
    trade_id = Column(String)
    trader = Column(String)
    symbol = Column(String)
    quantity = Column(Integer)
    notional = Column(Float)
    trade_type = Column(String)
    ccy = Column(String)
    counter_party = Column(String)
    low = Column(Float)
    high = Column(Float)
    price = Column(Float)


class Portfolio(Base):
    __tablename__ = 'portfolio_positions'
    pos_id = Column(String, primary_key=True)
    cob_date = Column(String)
    trader = Column(String)
    symbol = Column(String)
    ccy = Column(String)
    net_quantity = Column(Integer)
    net_amount = Column(Float)


def create_tables():
    Base.metadata.create_all(bind=engine)
print('Finished running db.py!')