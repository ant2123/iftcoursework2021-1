
--Industry volume in 2020
SELECT 
	AVG(volume) AS avg_vol, 
	s.GICSIndustry,
	s.GICSSector,
	'2020' AS year
FROM equity_prices AS p
LEFT JOIN equity_static AS s
	ON p.symbol_id = s.symbol
WHERE cob_date LIKE '%2020%'
GROUP BY GICSIndustry
ORDER BY avg_vol DESC;

--Industry volume in 2021
SELECT 
	AVG(volume) AS avg_vol, 
	s.GICSIndustry,
	s.GICSSector,
	'2021' AS year
FROM equity_prices AS p
LEFT JOIN equity_static AS s
	ON p.symbol_id = s.symbol
WHERE cob_date LIKE '%2021%'
GROUP BY GICSIndustry
ORDER BY avg_vol DESC;


--Which security is in trader's portfolio

WITH new_table AS (
	SELECT 
		AVG(volume) AS avg_vol, 
		s.symbol,
		s.security,
		s.GICSIndustry,
		s.GICSSector
	FROM equity_prices AS p
	LEFT JOIN equity_static AS s
	ON p.symbol_id = s.symbol
	GROUP BY p.symbol_id
	ORDER BY avg_vol DESC)
	-- Use the new table to inner join with the portiforlio table
SELECT 
	pos.trader,
	pos.net_amount, 
	pos.net_quantity,
	new_table.symbol, 
	new_table.security,
	new_table.avg_vol,
	new_table.GICSIndustry
FROM portfolio_positions as pos
INNER JOIN new_table 
	ON new_table.symbol = pos.symbol;




