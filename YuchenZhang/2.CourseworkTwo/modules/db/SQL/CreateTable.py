def SQLCreateSuspectTradeTable():
    Command = "CREATE TABLE trades_suspects (\
    trade_id TEXT CONSTRAINT suspect_pk PRIMARY KEY,\
    trade_time TEXT NOT NULL,\
    trader TEXT,\
    symbol TEXT,\
    quantity INTEGER,\
    notional INTEGER,\
    trade_type TEXT,\
    ccy TEXT,\
    counterparty TEXT,\
    FOREIGN KEY (symbol) REFERENCES equity_prices(symbol_id))"
    return (Command)

