# get all distinct TradeId
def historical_TradeId(historical_trades):
    historical_data = []
    for i in range(len(historical_trades)):
        historical_data.append(historical_trades[i].TradeId)
    return set(historical_data)

# get all historical Quantities
def historical_Quantity(historical_trades, new_trade):
    historical_data = []
    for i in range(len(historical_trades)):
        historical_data.append(historical_trades[i].Quantity)
    historical_data.append(new_trade.Quantity)
    return historical_data

# get all historical Quantities
def historical_price(historical_trades, new_trade):
    historical_data = []
    for i in range(len(historical_trades)):
        historical_data.append(historical_trades[i].Notional/historical_trades[i].Quantity)
    historical_data.append(new_trade.Notional/new_trade.Quantity)
    return historical_data

