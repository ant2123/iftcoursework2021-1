from modules.db.db_connection import mongo_connection

from static.model import Trade
from modules.utils.script_utils import read_config


conn_str, database, collection = read_config()
collection = mongo_connection(conn_str, database, collection)

async def fetch_one_trade(TradeId):
    document = await collection.find_one({"TradeId":TradeId})
    return document

async def fetch_all_trades():
    trades = []
    cursor = collection.find({})
    async for document in cursor:
        trades.append(Trade(**document))
    return trades

async def create_trade(trade):
    document = trade
    result = await collection.insert_one(document)
    return document

async def update_trade(TradeId, DateTime, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty):
    await collection.update_one({"TradeId": TradeId}, {"$set": {"DateTime": DateTime, "Trader": Trader, "Symbol": Symbol, "Quantity": Quantity, "Notional": Notional, "TradeType": TradeType, "Ccy": Ccy, "Counterparty": Counterparty}})
    document = await collection.find_one({"TradeId": TradeId})
    return document

async def remove_trade(TradeId):
    await collection.delete_one({"TradeId": TradeId})
    return True