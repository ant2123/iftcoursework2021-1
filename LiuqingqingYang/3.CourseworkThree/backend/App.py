from fastapi import FastAPI, Path, HTTPException
from fastapi.middleware.cors import CORSMiddleware
# from typing import Optional

from static.model import Trade
from modules.db.db_methods import (
    fetch_one_trade,
    # fetch_latest_trade,
    fetch_all_trades,
    create_trade,
    update_trade,
    remove_trade,
)
from modules.utils.function_utils import merge_suspects
from modules.utils.trade_utils import (
    historical_TradeId,
    historical_Quantity,
    historical_price
)


app = FastAPI()

origins  = ['http://localhost:3000', 'http://localhost:8000']

app.add_middleware(
    CORSMiddleware,
    allow_origins = origins,
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"],
)


@app.get("/")
def root():
    return {"Hello":"World"}

@app.get("/api/trade")
async def get_trade():
    response = await fetch_all_trades()
    return response

@app.get("/api/trade/{TradeId}", response_model=Trade)
async def get_trade_by_id(TradeId: str = Path(None, description='The TradeId of the trade you want to view.')):
    response = await fetch_one_trade(TradeId)
    if response:
        return response
    raise HTTPException(404, f"There is no trade with this trade id {TradeId}")

@app.post("/api/trade", response_model=Trade)
async def post_trade(trade:Trade):
    if trade.TradeId in historical_TradeId(await fetch_all_trades()):
        return{"Error": "Trade Exists"}
    elif ((trade.Quantity in merge_suspects(historical_Quantity(await fetch_all_trades(), trade))) |
    (trade.Notional/trade.Quantity in merge_suspects(historical_price(await fetch_all_trades(), trade)))):
        return{"Error": "Abnormal Trade Info"}
    response = await create_trade(trade.dict()) #convert JSON to dictionary
    if response:
        return response
    raise HTTPException(400, "Bad Request")

@app.put("/api/trade/{TradeId}", response_model=Trade)
async def put_trade(DateTime:str, TradeId:str, Trader:str, Symbol:str, Quantity:int, Notional:float, TradeType:str, Ccy:str, Counterparty:str):
    response = await update_trade(TradeId, DateTime, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty)
    if response:
        return response
    raise HTTPException(404, f"There is no trade with the trade id {TradeId}")

@app.delete("/api/trade/{TradeId}") 
async def delete_trade(TradeId):
    response = await remove_trade(TradeId)
    if response:
        return "Successfully deleted TODO"
    raise HTTPException(404, f"There is no trade with the trade id {TradeId}")

