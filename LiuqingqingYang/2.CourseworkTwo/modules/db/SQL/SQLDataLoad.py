#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/db/SQL/SQLDataLoad.py
#--------------------------------------------------------------------------------------

import sqlite3
import pandas as pd

# connect to sqllite3
def connect_sqlite(dbpath):
    conn = sqlite3.connect(dbpath)
    return conn

# read from sqllite3 and store in DataFrame
def read_sqlite(conn, query=''): 
    cu = conn.cursor()    
    df =  pd.DataFrame(list(cu.execute(query))) 
    df.columns = [i[0] for i in cu.description]
    return df

