#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/db/SQL/SQLDataWrite.py
#--------------------------------------------------------------------------------------

import pandas as pd

# write dataframe to sql table
def write_sqlite(data, tablename, conn, if_exists='append', index=False):
    try:
        if '_id' in data.columns:
            data['_id'] = data['_id'].astype(str)
        data.to_sql(tablename, conn, if_exists=if_exists, index=index)
    except:
        print('Primary key must be unique. Data already exists.')