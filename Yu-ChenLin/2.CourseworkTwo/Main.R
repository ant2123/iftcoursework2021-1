#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author : Yu-Chen Lin
# Topic : Main
# Case : detect incorrect trades(incorrect trade price or incorrect quantity) between 2021-11-11 & 2021-11-12
# Approach : trade vs price
#--------------------------------------------------------------------------------------

#install and library packages
install.packages("mongolite")
install.packages("lubridate")
install.packages("RSQLite")
install.packages("stringr")
library(mongolite)
library(lubridate)
library(dplyr)
library(RSQLite)
library(stringr)


# Parse ARGs ----------------------------------------------------------------------
Args = c("/Users/jenil/OneDrive/Documents/iftcoursework2021", "script.config", "script.params")

# Set working directory
setwd(Args[1])

# Source config
source(paste0("./Yu-ChenLin/2.CourseworkTwo/config/", Args[2]))
# Source params
source(paste0("./Yu-ChenLin/2.CourseworkTwo/config/", Args[3]))


# DataBase connection -------------------------------------------------------------------------------------
#-- SQL connection
ConSql <- dbConnect(RSQLite::SQLite(), DBConnection$SQLDataBase$Connection)

#-- MongoDB connection
ConMongo <- mongo(DBConnection$MongoDataBase[[1]],DBConnection$MongoDataBase[[2]],DBConnection$MongoDataBase[[3]])

options(scipen = 999)

# MongoDB Query
CW2 <- ConMongo$find(query = "{}") # importing all values in R
#View(CW2) # total 4400 observations
head(CW2) # see first few rows in our dataframe


#filter date "2021-11-11 & 2021-11-12" (from MongoDB), and calulate newPrice(=Notional/Quantity)
#filter date "2021-11-11" and create a column with new price
Date1111 <- CW2  %>%
  filter(DateTime >= "ISODate(2021-11-11T00:00:00.000Z)" &  DateTime < "ISODate(2021-11-12T00:00:00.000Z)") %>% 
  mutate(newPrice = Notional / Quantity)

#filter date "2021-11-12" and create a column with new price
Date1112 <- CW2  %>%
  filter(DateTime >= "ISODate(2021-11-12T00:00:00.000Z)" &  DateTime < "ISODate(2021-11-13T00:00:00.000Z)") %>% 
  mutate(newPrice = Notional / Quantity)

----------------------------------------------------------------------------------------------------------------------
  
# connect SQL
#variablePath <- "/your/path/toGitRepository" # amend this to reflect where you have cloned the git repository on your local machine

#select stock symbol, stock prices(high and low), Date on 2021-11-11 #from SQL
equityPrice1111 <- dbGetQuery(ConSql, "SELECT symbol_id,high,low FROM equity_prices WHERE cob_date = '11-Nov-2021'")
#left join 
New11 = merge(x = Date1111, y = equityPrice1111, by.x = "Symbol", by.y = "symbol_id", all.x = TRUE)

#detect incorrect trade price on 2021-11-11
#target:to see if newPrice(=Notional / Quantity) is between high and low
detect1111 = New11 %>% 
  mutate(CorrectorWrong = ifelse(newPrice>=low & newPrice<= high,"Correct","Wrong"))%>% 
  filter(CorrectorWrong == "Wrong")


#select Stock symbol, stock prices(high and low), Date on 2021-11-12
equityPrice1112 <- dbGetQuery(ConSql, "SELECT symbol_id,high,low FROM equity_prices WHERE cob_date = '12-Nov-2021'")
#left join
New12 = merge(x = Date1112, y = equityPrice1112, by.x = "Symbol", by.y = "symbol_id", all.x = TRUE)

#detect incorrect trade price on 2021-11-12
#target:to see if newPrice(=Notional / Quantity) is between high and low
detect1112 = New12%>% 
  mutate(CorrectorWrong = ifelse(newPrice>=low & newPrice<= high,"Correct","Wrong"))%>% 
  filter(CorrectorWrong == "Wrong")

#combine the trades that have problems in these two days(2021-11-11 and 2021-11-12)
detect_suspects = rbind(detect1111, detect1112)

# create a new table in SQLite Database called trades_suspects-->see more information in this file "CreateTable.R"

---------------------------------------------------------------------------------------------------------------------

#aggregate Quantity and Notional for all trades by date, trader, symbol, ccy and insert the results into portfolio_positions
New_Date1111 <- Date1111 %>% 
  mutate(DateTime=as.Date(str_sub(DateTime, start = 9, end = 18) ))
New_Date1112<- Date1112 %>% 
  mutate(DateTime=as.Date(str_sub(DateTime, start = 9, end = 18) ))


aggregate1111 <- aggregate(cbind(Quantity, Notional)~ DateTime+Trader+Symbol+Ccy,New_Date1111, sum ) %>% 
  mutate(pos_id=paste(Trader,format(DateTime,"%Y%m%d"),Symbol, sep=""), cob_date = format(DateTime,"%d-%b-%Y")) %>% 
  rename(net_quantity=Quantity,net_amount=Notional,trader = Trader, symbol = Symbol, ccy = Ccy) %>% 
  select(pos_id, cob_date,trader,symbol,ccy,net_quantity,net_amount) 

aggregate1112 <- aggregate(cbind(Quantity, Notional)~ DateTime+Trader+Symbol+Ccy,New_Date1112, sum ) %>% 
  mutate(pos_id=paste(Trader,format(DateTime,"%Y%m%d"),Symbol, sep=""), cob_date = format(DateTime,"%d-%b-%Y")) %>% 
  rename(net_quantity=Quantity,net_amount=Notional,trader = Trader, symbol = Symbol, ccy = Ccy) %>% 
  select(pos_id, cob_date,trader,symbol,ccy,net_quantity,net_amount) 

agg_11111112 <- rbind(aggregate1111,aggregate1112)

dbWriteTable(ConSql, "portfolio_positions", agg_11111112 , append=TRUE, row.names=FALSE)