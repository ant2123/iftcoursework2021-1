

def check_policy1(df, limit_dict):

    breaches = []
    for index, agg in df.iterrows():
        # print(agg)
        # consider only 2021-11-11 and 2021-11-12
        if agg["Date"] != "2021-11-11" and agg["Date"] != "2021-11-12":
            continue


        long_limit = limit_dict[(agg["Trader"], "long", "consideration", agg["Currency"])]
        short_limit = limit_dict[(agg["Trader"], "short", "consideration", agg["Currency"])]
        
        # print(agg, long_limit[0])
        
        if agg["SumNotional"] > long_limit[0]:
            # print("### Long consideration breach")
            # print(agg)
            # print(limit_dict[(agg["Trader"], "long", "consideration", agg["Currency"])])
            # print()
            breaches.append((1, long_limit[1], agg["SumNotional"], agg["Date"]))
        if agg["SumNotional"] < -1 * short_limit[0]:
            # print("Short consideration breach")
            breaches.append((1, short_limit[1], agg["SumNotional"], agg["Date"]))

    return breaches
    


