# Breaches
- Volatility
  - volatility 的计算公式需要再对一下 => std of close
  - for each stock
- Sector

# Selected Trader

For each stock A
- Capital gain: average( (第二天 close / 第一天 close) / 第一天 close )
- mean: average (stock A close)
- SD: std (stock A close) => volatility
  - bar chart
- stock exposure (%) stock / all
- % change: 第二天 close - 第一天 close / 第一天 close
  - 折线图
- % return 盈亏: (卖出 - 买入) / 买入
  - top 10
  - bottom 10


- Risk-free rate: 5%
- beta
- Alpha: depends on beta


Portfolio (all stock)
- portfolio return: 
  - weight of A = stock exposure of A
  - 盈亏 => (卖出 - 买入) / 买入
- net_quantity
- net_amount
- 
- long exposure (%): long / 全部 trade 百分之多少的钱去做 long
- short exposure (%): short / 全部 trade 百分之多少的钱去做 short
  - pie chart
- net exposure (%): long exposure - short exposure

- portfolio volatility: weighted SD
- sharpe =（portfolio return-risk free）/portfolio volatility



Every two stocks
- Correlation: heap map
- portfolio SD: heap map


- average daily change: 平均值
- average captial gain


TODO:
- beta
- alpha
- portfolio return
  
- expect return
- excess return

- sortino ratio
- sharpe ratio
- treynor ratio
- information ratio