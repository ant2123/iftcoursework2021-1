from scipy.stats import norm
import pandas as pd
import numpy as np

# Policy 3: sector relative: max concentration of portfolio allocations by sector. all stocks in a portfolio for a sector cannot exceed the X% specified. this is the result of sum portfolio value as quantity times market price (mark to market positions) by sector over the total mark to market portfolio value.

# join the sector information to the aggregated position
def join_equity_static(df, con):
    equity_static = pd.read_sql_query(f"SELECT * FROM equity_static", con)

    # print(equity_static)
    
    # join equity_static and df
    joined_df = df.join(equity_static.set_index('symbol'), on="Symbol")
    # print(df, joined_df)

    return joined_df.groupby(['Trader', 'Currency', 'Date', 'GICSSector'])['SumNotional'].sum()

def sum_stock(df):
    return df.groupby(['Trader', 'Currency', 'Date'])['SumNotional'].sum()

def check_policy3(df, limit_dict, con):
    agg_df = join_equity_static(df, con)
    df2 = sum_stock(df)


    breaches = []
    for id, agg in agg_df.items():
        # print(df2[id])
        sector_sum = df2[(id[0], id[1], id[2])]
    
        # consider only 2021-11-11 and 2021-11-12
        if id[2] != "2021-11-11" and id[2] != "2021-11-12":
            continue
        sector_relative_limit = limit_dict[(id[0], "sector", "relative", "PCG")]
        sector_relative = agg / sector_sum * 100

        if sector_relative > sector_relative_limit[0]:
            breaches.append((3, sector_relative_limit[1], "N/A", sector_relative, id[2]))

    return breaches