from fastapi import FastAPI

from modules.DataImport.TradeModel import Trade
from modules.DataProcessing.SanityCheck import sanityCheck
from modules.DataOutput.OutputToMongoDB import outputToMongoDB

app = FastAPI()


@app.post("/submit_trade/")
async def submitTrade(trade: Trade):
    accept=sanityCheck(trade)
    if accept:
        outputToMongoDB(trade)
        return "Accepted"
    else:
        return "Rejected"








