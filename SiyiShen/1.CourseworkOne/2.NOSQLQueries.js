
// Query 1 for NoSQL


db.CourseworkOne.find({"MarketData.Price": {"$lt": 81.27}}).pretty()


// Query 2 for NoSQL

db.CourseworkOne.find({"FinancialRatios.DividendYield": {"$gte": 1.6, "$lte":3.3  }}).count()                                                                                 