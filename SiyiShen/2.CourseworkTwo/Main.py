import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sqlalchemy import create_engine

# open connectivity to MongoDB
con = MongoClient('mongodb://localhost')
db = con.Equity
table_data = db.CourseworkTwo


# MongoDB Query
all_data = table_data.find({}) 
df = pd.DataFrame(all_data)

print(df.head())
print(df.info())

def get_date(s):
    return s[8:18]
df['DateTime_date'] = df["DateTime"].apply(get_date)

def box_plot(col):
    low = col.quantile(0.25)-1.5*(col.quantile(0.75)-col.quantile(0.25))
    up = col.quantile(0.75)+1.5*(col.quantile(0.75)-col.quantile(0.25))
    drop_index = col.loc[(col < low) | (col > up)].index
    return drop_index, len(drop_index)

engine = create_engine(f"sqlite:///D:/69Big Data/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()

save_TradeId = []
for date_str in ['2021-11-11', '2021-11-12']:
    df1 = df[df['DateTime_date']==date_str]
    
    for col in ['Quantity','Notional']:
        df2 = df1[[col]]
        plt.figure(figsize=(15,7)) 
        p = df2.boxplot() 
        plt.savefig('result/%s %s boxplot.png' % (date_str, col))
        plt.show()
        
        box_res = box_plot(df1[col])
        df_res = df1.loc[box_res[0]]
        # print(df1[col][box_res[0]])
        df_res.index = range(len(df_res))
        df_res.to_excel('result/%s %s boxplot.xlsx' % (date_str, col))
        
        for i in range(len(df_res)):
            if df_res.loc[i,"TradeId"] not in save_TradeId:
                save_TradeId.append(df_res.loc[i,"TradeId"])
                con.execute(f'INSERT INTO trades_suspects (DateTime, TradeId, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty) \
                            VALUES ("{df_res.loc[i,"DateTime"]}",\
                            "{df_res.loc[i,"TradeId"]}",\
                            "{df_res.loc[i,"Trader"]}",\
                            "{df_res.loc[i,"Symbol"]}",\
                            {df_res.loc[i,"Quantity"]},\
                            {df_res.loc[i,"Notional"]},\
                            "{df_res.loc[i,"TradeType"]}",\
                            "{df_res.loc[i,"Ccy"]}",\
                            "{df_res.loc[i,"Counterparty"]}")')
con.close()
