import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sqlalchemy import create_engine

# open connectivity to MongoDB
con = MongoClient('mongodb://localhost')
db = con.Equity
table_data = db.CourseworkTwo


# MongoDB Query
all_data = table_data.find({}) 
df = pd.DataFrame(all_data)

def get_date(s):
    return s[8:18]
df['DateTime_date'] = df["DateTime"].apply(get_date)

# •	aggregate Quantity and Notional for all trades by date, trader, symbol, ccy a
# traderid + cob_date (Ymd) + symbol_id 
df_group = df.groupby(['DateTime_date', 'Trader','Symbol','Ccy']).sum().reset_index()


engine = create_engine(f"sqlite:///D:/69Big Data/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()

for i in range(len(df)):
    try:
        pos_id = df['Trader'][i]+df['DateTime_date'][i].replace('-','')+df['Symbol'][i]+df['Ccy'][i]
        con.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol,ccy, net_quantity, net_amount) \
                                VALUES ("{pos_id}",\
                                "{df_group.loc[i,"DateTime_date"]}",\
                                "{df_group.loc[i,"Trader"]}",\
                                "{df_group.loc[i,"Symbol"]}",\
                                "{df_group.loc[i,"Ccy"]}",\
                                {df_group.loc[i,"Quantity"]},\
                                {df_group.loc[i,"Notional"]})')
    except:
        continue
con.close()