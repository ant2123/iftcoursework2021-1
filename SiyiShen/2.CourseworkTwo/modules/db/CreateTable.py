from sqlalchemy import create_engine

def SQLCreateRetTable():
    Command = "CREATE TABLE trades_suspects (\
    DateTime TEXT NOT NULL,\
    TradeId TEXT NOT NULL,\
    Trader TEXT NOT NULL,\
    Symbol TEXT,\
    Quantity INTEGER,\
    Notional REAL,\
    TradeType TEXT,\
    Ccy TEXT,\
    Counterparty TEXT,\
    PRIMARY KEY (TradeId),\
    FOREIGN KEY (Trader) REFERENCES trader_static(trader_id))"
    return (Command)


engine = create_engine(f"sqlite:///D:/69Big Data/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()
con.execute(SQLCreateRetTable())

con.close()