#-------------------------------------------------
#-------------------------------------------------
# Farid Rajabov - best code in the world :)
#-------------------------------------------------
# Coursework Two - Case One - Main
#-------------------------------------------------

#%%

# 1. Importing stuff ======================================================================================================================


from pymongo import MongoClient
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import modules.CreateTable 


# 2. Getting Data from DataBases into DataFrames ======================================================================================================================


# 2.1 NoSQL

# Connecting to MongoDB
conmo = MongoClient(port=27017)

# Accessing our Data
db = conmo.Equity
col = db.CourseworkTwo

# Putting the Data in DataFrame
nsqdb = pd.DataFrame(list(col.find())) 


# 2.2 SQL

# Connecting to SQLite
engsq = create_engine(f"sqlite:///C:/Users/Farid/Documents/UCL_studies_docs/BigData/Equity.db")
consq=engsq.connect()

# Putting the Data in DataFrame
sqdb = pd.read_sql_query(
    "SELECT\
    equity_prices.symbol_id AS Symbol,\
    equity_prices.cob_date,\
    equity_prices.high,\
    equity_prices.low\
    FROM equity_prices\
    WHERE cob_date= '11-Nov-2021'\
    OR\
    cob_date= '12-Nov-2021'"
    ,engsq)


# 3. Data Sorting & Union ============================================================================================================


# 3.1 Nov 11
# 3.1.1 Nov 11 - NoSQL Trades 
nsqdb11 = nsqdb[nsqdb['DateTime'].str.contains('2021-11-11')]
nsqdb11['Stock Price'] = nsqdb11['Notional'] / nsqdb11['Quantity']
nsqdb11=nsqdb11[['Symbol','TradeId','Trader','DateTime','Stock Price']]

# 3.1.2 Nov 11 - SQL Data 
sqdb11 = sqdb[sqdb['cob_date'].str.contains('11-Nov')]

# 3.1.3 Nov 11 - Union 
Un_11m = pd.merge(nsqdb11, sqdb11, on=["Symbol"])

# 3.1.4 Nov 11 - Check Trades 
SuspectornotCondition = [(Un_11m['Stock Price'] < Un_11m['low']), (Un_11m['Stock Price'] > Un_11m['high']),
(Un_11m['Stock Price'] > Un_11m['low']) & (Un_11m['Stock Price'] < Un_11m['high'])]
Suspectornot = ['Suspicious', 'Suspicious','Okay']
Un_11m['Suspicion Status'] = np.select(SuspectornotCondition,Suspectornot)

# 3.1.5 Nov 11 - Add Trades to Table 
Susp11 = Un_11m[Un_11m['Suspicion Status'] =='Suspicious']

# ---------------------------------------------------------------------------------------

# 3.2 Nov 12
# 3.2.1 Nov 12 - NoSQL Trades 
nsqdb12 = nsqdb[nsqdb['DateTime'].str.contains('2021-11-12')]
nsqdb12['Stock Price'] = nsqdb12['Notional'] / nsqdb12['Quantity']
nsqdb12 = nsqdb12[['Symbol','TradeId','Trader','DateTime','Stock Price']]

# 3.2.2 Nov 12 - SQL Data 
sqdb12 = sqdb[sqdb['cob_date'].str.contains('12-Nov')]

# 3.2.3 Nov 12 - Union 
Un_12m = pd.merge(nsqdb12, sqdb12, on=["Symbol"])

# 3.2.4 Nov 12 - Check Trades 
SuspectornotCondition = [(Un_12m['Stock Price'] < Un_12m['low']), (Un_12m['Stock Price'] > Un_12m['high']),
    (Un_12m['Stock Price'] > Un_12m['low']) & (Un_12m['Stock Price'] < Un_12m['high'])]
Suspectornot = ['Suspicious', 'Suspicious','Okay']
Un_12m['Suspicion Status'] = np.select(SuspectornotCondition,Suspectornot)

# 3.2.5 Nov 12 - Add Trades to Table 
Susp12 = Un_12m[Un_12m['Suspicion Status'] == 'Suspicious']


# 4. All Suspect Trades ============================================================================================================


AllSus = Susp11.append(Susp12)
AllSus = AllSus.drop_duplicates(subset='TradeId', keep='first').reset_index(drop=True)


# 5. Add Suspect Trades to SQL ============================================================================================================


# 5.1 Create a table

if engsq.dialect.has_table(consq, "trades_suspects") == True:
    pass
else:
    consq.execute(modules.CreateTable.CrtSusTblSQL())

# 5.2 Put Suspected Trades in a Table

for i in range(len(AllSus)):
    consq.execute(f'INSERT INTO trades_suspects (Symbol, TradeId, Trader, cob_date, Stock_Price, high, low, Suspicion_Status) VALUES (\
        "{AllSus.loc[i,"Symbol"]}",\
        "{AllSus.loc[i,"TradeId"]}",\
        "{AllSus.loc[i,"Trader"]}",\
        "{AllSus.loc[i,"cob_date"]}",\
        {AllSus.loc[i,"Stock Price"]},\
        {AllSus.loc[i,"high"]},\
        {AllSus.loc[i,"low"]},\
        "{AllSus.loc[i,"Suspicion Status"]}")')


# 6. Aggregate Quantity and Notional of Mongo ============================================================================================================


# 6.1 Putting the Data in DataFrame
nsqdb = pd.DataFrame(list(col.find())) 

# 6.2 'Trades To Aggregate' from Mongo
tta = nsqdb[['DateTime','Trader','Symbol','Ccy','Quantity','Notional']]

# 6.3 Preparing the Date data
tta['cob_date'] = tta['DateTime'].str.split("T", 1).str[0].str.split("(", 1).str[1]

# 6.4 date column for pos_id
tta['pos_id_date'] = tta['cob_date'].str.replace('-','')

# 6.5 Create pos_id
tta['pos_id'] = tta['Trader'] + tta['pos_id_date'] + tta['Symbol']
tta=tta[['pos_id','cob_date','Trader','Symbol','Ccy','Quantity','Notional']]

# 6.6 Finishing 'AggregatedTradesToAggregate' table
aggtta = tta.groupby(['pos_id', 'cob_date', 'Trader', 'Symbol', 'Ccy']).agg(Quantity = ('Quantity','sum'),Notional = ('Notional', 'sum')).reset_index()


# 7. Final upload of aggregated table to SQL ============================================================================================================


for i in range(len(aggtta)):
    consq.execute(f'INSERT OR IGNORE INTO portfolio_positions\
        (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)\
        VALUES ("{aggtta.loc[i,"pos_id"]}",\
        "{aggtta.loc[i,"cob_date"]}",\
        "{aggtta.loc[i,"Trader"]}",\
        "{aggtta.loc[i,"Symbol"]}",\
        "{aggtta.loc[i,"Ccy"]}",\
        {aggtta.loc[i,"Quantity"]},\
        {aggtta.loc[i,"Notional"]})')


print("All Tasks Finished")

consq.close()

#%%