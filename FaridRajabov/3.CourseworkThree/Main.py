#-------------------------------------------------
#-------------------------------------------------
# Farid Rajabov
#-------------------------------------------------
# Coursework Three - Case One
#-------------------------------------------------



#%% 0. Importing stuff
from cgitb import html
from re import X
from symtable import Symbol
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import matplotlib.pyplot as plt

pd.set_option('display.max_rows', 70)

# 1. take only date from & till, that exists for portfolio - 2020-10-06 till 2021-11-12 ====================
engsq = create_engine(f"sqlite:///C:/Users/Farid/Documents/UCL_studies_docs/BigData/Equity.db")
consq=engsq.connect()
# 1.1 Relevant information from SQL
sqdb = pd.read_sql_query("\
    SELECT\
    equity_prices.cob_date,\
    equity_prices.close,\
    equity_prices.symbol_id\
    FROM\
    equity_prices\
    ",engsq)

snms = pd.read_sql_query("\
    SELECT\
    equity_static.symbol,\
    equity_static.security,\
    equity_static.GICSIndustry\
    FROM\
    equity_static\
    ",engsq)

# 1.2 Preparing the Date & Pos_id data
sqdb['cob_date'] = pd.to_datetime(sqdb['cob_date'])
sqdb['cob_date'] = sqdb['cob_date'].astype(str)
sqdb['pos_id_date'] = sqdb['cob_date'].str.replace('-','')
sqdb['pos_id'] = sqdb['symbol_id'] + sqdb['pos_id_date']
sqdb = sqdb[['cob_date','close','symbol_id','pos_id']]
prcs = sqdb.sort_values(by='pos_id', ascending=True).reset_index(drop=True)


# 2. Create DataFrame End of the Day stocks Quantity in Portfolio ==========================================
conmo = MongoClient(port=27017) 
db = conmo.Equity
col = db.CourseworkTwo
nsqdb = pd.DataFrame(list(col.find())) 
# 2.1 Relevant information from Mongo
tta = nsqdb[['DateTime','Trader','TradeType','Symbol','Quantity','Notional','Ccy','_id','TradeId']]

# 2.2 Singling out our trader
tta = tta[tta['Trader'].str.contains('DGR1983')].reset_index(drop=True)
LDPrtfD = tta

# 2.3 Preparing the Date & Pos_id data
tta['cob_date'] = tta['DateTime'].str.split("T", 1).str[0].str.split("(", 1).str[1]
tta = tta[['cob_date','Trader','Symbol','Quantity','Notional']]
tta['pos_id_date'] = tta['cob_date'].str.replace('-','')
tta['pos_id'] = tta['Symbol'] + tta['pos_id_date']
tta = tta[['cob_date','Trader','Symbol','Quantity','Notional','pos_id']]

# 2.4 Net Portfolio Positions with every change
aggtta = tta.groupby(['cob_date','Symbol','pos_id']).agg(Quantity = ('Quantity','sum'),Notional = ('Notional', 'sum')).reset_index()
aggtta['This Day Quantity'] = aggtta.groupby('Symbol')['Quantity'].cumsum()

Prtf = aggtta.sort_values(by='pos_id', ascending=True).reset_index(drop=True)

# 3. Union  ======================================================================

# 3.1 Only Prices relevant to Portfloio
unar = aggtta['Symbol'].unique()
unls = unar.tolist()
prcspr = prcs.loc[prcs['symbol_id'].isin(unls)]
prcspr = prcspr.sort_values(by='pos_id', ascending=True).reset_index(drop=True)

# 3.2 Union based of Market Prices

prtnprc = pd.merge(prcspr,
    Prtf[['cob_date', 'Symbol', 'pos_id','This Day Quantity']],
    how = "left",
    on = 'pos_id')
prtnprc = prtnprc[['cob_date_x','close','symbol_id','pos_id','This Day Quantity']]

# 3.3 Fixing the Data, becuase we only had the dates the changes to portfolio were made
prtnprc['This Day Quantity'] = prtnprc['This Day Quantity'].fillna(prtnprc.groupby('symbol_id')['This Day Quantity'].ffill())
prtnprc = prtnprc.dropna().reset_index(drop=True)

# 3.4 Cleaning Talbe
PrtVal = prtnprc
PrtVal['Date'] = prtnprc['cob_date_x']
PrtVal['This Day Price'] = prtnprc['close']
PrtVal['Symbol'] = prtnprc['symbol_id']
PrtVal = prtnprc[['pos_id','Date','Symbol','This Day Quantity','This Day Price']]

# 3.5 Obtaining Portfolio's Value for everyday available by data
PrtVal['Net Value'] = PrtVal['This Day Quantity'] * PrtVal['This Day Price']

# 4. Last Day positions ------------------------------------------------------------------------
LDPrtf = LDPrtfD.groupby(['Symbol']).agg(NetQuantity = ('Quantity','sum'),NetNotional = ('Notional', 'sum')).reset_index()

# 4.1 Preparing Last Day Portfolio Positions
LDPrtf['Total Notional'] = LDPrtf['NetNotional'].sum() 
LDPrtf['PCT of Total Portfolio'] = (LDPrtf['NetNotional']/LDPrtf['Total Notional'])
pd.set_option('display.float_format', lambda x: '%.3f' % x)
LDPrtf = LDPrtf.sort_values('PCT of Total Portfolio',ascending=False).reset_index(drop=True)

# 4.2 Obtaining Names & Industry
snms['Symbol'] = snms['symbol']
snms.drop(columns=['symbol'])

LDPrtfwN = pd.merge(LDPrtf, snms,
    how = "left", on = 'Symbol')
LDPrtfwN = LDPrtfwN.drop(columns=['symbol','Total Notional'])

LDPrtfwN = LDPrtfwN.sort_values('Symbol').reset_index(drop=True)

# 4.3 Calculating a Year long Return on Investment for Stocks in our portfolio
pd.set_option('display.max_rows', 30000)

Poiroi20 = prcspr[prcspr['cob_date'].str.contains('2020-05-14')].reset_index(drop=True)
Poiroi20['open'] = Poiroi20['close']
Poiroi20 = Poiroi20.drop(columns=['close'])

Poiroi21 = prcspr[prcspr['cob_date'].str.contains('2021-05-14')].reset_index(drop=True)
Poiroi21

poiroiun = pd.merge(Poiroi20, Poiroi21,
    how = "left", on = 'symbol_id')
poiroiun['Year Start Log'] = np.log(poiroiun['open'])
poiroiun['Year Finish Log'] = np.log(poiroiun['close'])
poiroiun = poiroiun.drop(columns=['cob_date_x','open','close','pos_id_x','cob_date_y','pos_id_y'])
poiroiun['Return On Investment'] = poiroiun['Year Finish Log'] - poiroiun['Year Start Log']
poiroiun['Symbol'] = poiroiun['symbol_id']
poiroiun = poiroiun.drop(columns=['Year Finish Log','Year Start Log','symbol_id'])

Port1 = pd.merge(LDPrtfwN, poiroiun,
    how = "left", on = 'Symbol')

Port1 = Port1.sort_values('Symbol').reset_index(drop=True)

# 4.4 Calculate Stocks' Standart Deviations
StStd = prcspr.groupby(['symbol_id']).std()
w = np.array(Port1['PCT of Total Portfolio']).reshape(-1,1)
s = np.array(StStd['close']).reshape(-1,1)
ws = (w * s).sum()

Port1['Weighted ROI'] = Port1['Return On Investment'] * Port1['PCT of Total Portfolio'] 
SRPrat = (Port1['Weighted ROI'].sum())/ws
SRPratio = SRPrat*100

# Taking Top 10 Positions
PrTp = Port1.sort_values('PCT of Total Portfolio',ascending=False).reset_index(drop=True)
PrTp['PCT of Total Portfolio'] = PrTp['PCT of Total Portfolio']*100

StStd = prcspr.groupby(['symbol_id']).std()
StStd['Symbol'] = StStd.index
StStd = StStd.reset_index(drop=True)
StStd['Price Standart Deviation'] = StStd['close']
StStd = StStd.drop(columns = ['close'])

PrTp = pd.merge(PrTp, StStd,
    how = "left", on = 'Symbol')

PrTp = PrTp.drop(columns=['Symbol'])
PrTp = PrTp.set_index('security')

PrTp10 = PrTp.head(10)

# 5. Plotting ---------------------------------------------------------------------------------

# 5.1 Overall Portfolio History

# 5.1.1 Date & Net Value --------------------------------------------------------------
PrtNV = PrtVal.groupby(['Date']).agg(NetValue = ('Net Value','sum')).reset_index()
PrtNV = PrtNV.sort_values('Date').reset_index(drop=True)
PrtNVP = PrtNV.set_index('Date')
PrtNVP['NetValue in 100M'] = PrtNVP['NetValue']/100000000
PrtNVP['NetValue'].plot()
plt.ylabel("$ 100 Millions Portfolio Value")
plt.savefig('PrtNVP.png')

# 5.1.2 Before Boom - in 1.5 years ----------------------------------------------------
pltPVs = PrtNVP.loc[PrtNVP['NetValue in 100M'] < 0.9]
pltPVs['NetValue in 100M'].plot()
plt.ylabel("$ 100 Millions Portfolio Value")
plt.savefig('pltPVs.png')

# 5.1.3 After Boom - in one month ----------------------------------------------------
pltPVb = PrtNVP.loc[PrtNVP['NetValue in 100M'] > 0.9]
pltPVb['NetValue in 100M'].plot()
plt.ylabel("$ 100 Millions Portfolio Value")
plt.savefig('pltPVb.png')


# 5.1.4 Date & Average Stocks in Portoflio Price --------------------------------------------------------------
PrtAVP = PrtVal.groupby(['Date']).agg(AveragePrice = ('This Day Price','mean')).reset_index()
pd.set_option('display.max_rows', 50)
PrtAVP = PrtAVP.sort_values('Date').reset_index(drop=True)
PrtAVPP = PrtAVP.set_index('Date')
PrtAVPP['AveragePrice'].plot()
plt.ylabel("$ Average Stocks in Portoflio Price")
plt.savefig('PrtAVPP.png')

# 5.1.5 Date & Millions of Stocks Owned ----------------------------------------------------
PrtSQ = PrtVal.groupby(['Date']).agg(Quantity = ('This Day Quantity','sum')).reset_index()
PrtSQ = PrtSQ.sort_values('Date').reset_index(drop=True)
PrtSQP = PrtSQ.set_index('Date')
PrtSQP['Quantity'].plot()
plt.ylabel("Millions of Stocks Owned")
plt.savefig('PrtSQP.png')
#%%
# 5.1.6 Top Positions Pie
PrTp10P = PrTp10['PCT of Total Portfolio']
plt.pie(PrTp10P)
plt.pie(PrTp10P, labels = PrTp10P.index)
plt.draw()
plt.savefig('PrTp10P.png')


# 6. Writing HTML report -------------------------------------------------------------------------
page_title_text = 'Analytics Report - Dan Green (DGR1983)'
title_text = 'The Details of Main Allocations and Historical Trends of trader Dan Green (DGR1983)'
text1 = 'Farid Rajabov'
text2 = 'The Details of Main Allocations'

ontable = 'Dan Green Portfolio holds 62 positions, Sharpe ratio = 0.7'
top10text = 'Top 10 Positions Held: '
text3 = 'Steep increase on Date & Net Portfolio Value graph shows sudden increase in portfolio value at a later point.'
text4 = 'This is not due to increase in prices, which can be tracked in Date & Average Stocks in Portoflio Price graph.'
text5 = 'Prices on average increase steadily.' 
text6 = 'Quantity however, have seen drastic increase at exactly the same point as portfolio value'
text7 = 'That likely means that Mr Green took leverage that allowed him for such a drastic increase in investment value'

html = f'''
    <html>
        <head>
            <title>{page_title_text}</title>
        </head>
        <body>
                <h1>{title_text}</h1>
                <p>{text1}</p>
                <h2>{text2}</h2>
                {PrTp.to_html()}
                <h2>{ontable}</h2>
                <p>{top10text}</p>
                <img src='PrTp10P.png' width="500">
                <p>{text3}</p>
                <p>{text4}</p>
                <p>{text5}</p>
                <p>{text6}</p>
                <p>{text7}</p>
                <img src='PrtNVP.png' width="500">
                <img src='pltPVs.png' width="500">
                <img src='pltPVb.png' width="500">
                <img src='PrtAVPP.png' width="500">
                <img src='PrtSQP.png' width="500">
        </body>
    </html>
    '''

with open('Farid_report.html', 'w') as f:
    f.write(html) 

print("HTML report is Successfully Generated")
#
# %%
