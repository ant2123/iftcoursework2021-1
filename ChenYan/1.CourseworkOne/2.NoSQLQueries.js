use Equity
db.createCollection("CourseworkOne")
db.CourseworkOne.insert()


db.CourseworkOne.distinct("StaticData.GICSSector")


db.CourseworkOne.find({"StaticData.GICSSector": "Communication Services"}).limit(2).pretty()


db.CourseworkOne.aggregate([
{$match: {} },
{$group: {_id: "$StaticData.GICSSector", count: {$sum:1} }}])


db.CourseworkOne.aggregate([
{$match: {"MarketData.MarketCap": {"$gte": 50000} } },
{$group: {_id: "$StaticData.GICSSector", avgBeta: {$avg:"$MarketData.Beta"} }},
{$sort:{avgBeta:1}}])


db.CourseworkOne.aggregate([
    {$match: {"StaticData.GICSSector": "Information Technology"} },
    {$group: {_id: "$StaticData.GICSSubIndustry", avg_Price: {$avg: "$MarketData.Price"} } }])
 
