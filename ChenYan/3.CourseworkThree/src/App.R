#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chen Yan
# Topic   : Use Case Three.a (R) - visualising daily positions by trader.
# File    : App.R
#--------------------------------------------------------------------------------------

# install packages needed if not yet installed
if(!any(c("ggplot2","dplyr", "lubridate", "RSQLite","shiny","DT") %in% installed.packages()[,1])){
  install.packages("ggplot2")
  install.packages("dplyr")
  install.packages("lubridate")
  install.packages("RSQLite")
  install.packages("shiny")
  install.packages("DT")
} 

#import packages
library(ggplot2)
library(dplyr)
library(lubridate)
library(RSQLite)
library(shiny)
library(DT)

# important to set up directories
GITRepoDirectory <- "/Users/YanC/Downloads/Study/iftcoursework2021" 

# source config
source(paste0(GITRepoDirectory, "/ChenYan/3.CourseworkThree/config/script.config"))

# setting up connection for SQLite
source(paste0(GITRepoDirectory, Config$Directories$Connection, "/db.connection.R"))

# import data from SQLite
source(paste0(GITRepoDirectory, Config$Directories$SQLQuery, "/SQLQueries.R"))
fullPortfolio <- as.data.frame(dbGetQuery(conSql, SQLQueryPortfolio ))
fullStatic <- as.data.frame(dbGetQuery(conSql, SQLQueryStatic ))
fullPrice <- as.data.frame(dbGetQuery(conSql, SQLQueryPrice ))

# merging data and calculating markrt value for each transaction
for (i in 1:nrow(fullPortfolio)){
  fullPortfolio$GICSSector[i]<-fullStatic[which(fullPortfolio[i,"symbol"] == fullStatic$symbol),"GICSSector"]
  fullPortfolio$ClosePrice[i]<-fullPrice[which( fullPortfolio[i,"symbol"] == fullPrice$symbol_id  &  fullPortfolio[i,"cob_date"] == fullPrice$cob_date ),"close"]
  fullPortfolio$MarketValue[i]<-fullPortfolio$net_quantity[i]*fullPortfolio$ClosePrice[i]
}


# prepare data for visualization page
#create new column names
new_cols_table<-c('trader','cob_date','symbol','GICSSector','ccy','net_quantity','net_amount')
new_cols_exposure<-c('trader','cob_date','GICSSector','net_quantity','net_amount','MarketValue')

# source server
source(paste0(GITRepoDirectory, Config$Directories$shiny, "/server.R"))
# source ui
source(paste0(GITRepoDirectory, Config$Directories$shiny, "/ui.R"))

# run shiny
shinyApp(ui, server)

# disconnet SQLite 
dbDisconnect(conSql)

