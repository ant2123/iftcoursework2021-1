# Import relevant packages
import sqlite3
import pandas as pd
from pymongo import MongoClient
import numpy as np
import matplotlib as plt
import seaborn as sns
from jinja2 import Environment, FileSystemLoader

pd.set_option('display.expand_frame_repr', False)

# Set up SQL and Mongodb connection
sql_Equity = "/Users/andreaspantelides/Desktop/BigData/iftcoursework2021/000.DataBases/SQL/Equity.db"
con_sql = sqlite3.connect(sql_Equity)
cursor = con_sql.cursor()

mongoc = MongoClient('mongodb://localhost')
db = mongoc.Equity
colle = db.CourseworkTwo

# Retrieve all trades for November 12 specifically for trader DGR1983
Nov_12 = pd.DataFrame(colle.find({"DateTime": {"$gt": "ISODate(2021-11-12T00:00:00.000Z)", "$lte": "ISODate(2021-11-13T00:00:00.000Z)"}}))
Nov_12 = Nov_12.loc[Nov_12['Trader'] == "DGR1983"]
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(Nov_12)

print(Nov_12["TradeType"].unique())

# Summed all stocks by symbol and renamed columns
Nov_12_3_columns = Nov_12[['Symbol', 'Quantity', "Notional"]]
Nov_12_3_columns = Nov_12_3_columns.reset_index()
print(Nov_12["Symbol"].unique())
Nov_12_3_columns = Nov_12_3_columns.drop(Nov_12_3_columns.columns[0], axis=1)
Nov_12_3_columns = Nov_12_3_columns.groupby("Symbol").sum()
Nov_12_3_columns = Nov_12_3_columns.rename(columns={"Quantity": "Sum of Quantity", "Notional": "Sum of Notional"})

# Imported data for low high for each stock
# Then calculated the volatility of the low and high pricd of each stock on November 12
list_of_symbols = tuple(Nov_12["Symbol"].unique())
sql_Nov_12 = pd.read_sql(f"SELECT symbol_id, low, high, cob_date from equity_prices WHERE (cob_date = '12-Nov-2021' AND symbol_id IN {list_of_symbols})", con_sql)
print(sql_Nov_12)
sql_Nov_12["volatility"] = sql_Nov_12.apply(lambda row: np.std([row.low, row.high]), axis=1)
print(sql_Nov_12)

Nov_12_3_columns = Nov_12_3_columns.astype(int)
print(Nov_12_3_columns)

# Calculated mean and median for Sum of Quantity and Sum of Notional
q1 = f"Mean of Sum of Quantity:  {np.mean(Nov_12_3_columns['Sum of Quantity'])}"
print(q1)
s1 = f"Mean of Sum of Notional:  {np.mean(Nov_12_3_columns['Sum of Notional'])}"
print(s1)
q2 = f"Mean of Sum of Quantity:  {np.median(Nov_12_3_columns['Sum of Quantity'])}"
print(q2)
s2 = f"Mean of Sum of Notional:  {np.mean(Nov_12_3_columns['Sum of Notional'])}"
print(s2)

# Plotted Sum of Quantity against Sum of Notional
sns.scatterplot(x=Nov_12_3_columns["Sum of Quantity"], y=Nov_12_3_columns["Sum of Notional"])

plt.pyplot.scatter(Nov_12_3_columns["Sum of Quantity"], Nov_12_3_columns["Sum of Notional"])
plt.pyplot.xlabel("Sum of Quantity")
plt.pyplot.ylabel("Sum of Notional")
fig1 = plt.pyplot.gcf()
plt.pyplot.show()
plt.pyplot.draw()
fig1.savefig('modules/scatter.png', dpi=100)

env = Environment(loader=FileSystemLoader('/Users/andreaspantelides/Desktop/BigData/iftcoursework2021/AndreasPantelides/3.CourseworkThree/modules'))

template = env.get_template('report_template_2.html')

html = template.render(title_text="My report",
                       volatility_text="Volatility for each stock",
                       volatility=sql_Nov_12,
                       sum_text="Sum of Quantity and Sum of Notional",
                       sum=Nov_12_3_columns,
                       s1=s1,
                       q1=q1,
                       s2=s2,
                       q2=q2
                       )

with open("/Users/andreaspantelides/Desktop/BigData/iftcoursework2021/AndreasPantelides/3.CourseworkThree/test/Home.html", "w") as f:
    f.write(html)
