
def trade_fat_fingers():
    fat_fingers = "CREATE TABLE trade_suspects("\
                  " TradeId TEXT PRIMARY KEY, "\
                  " DateTime TEXT NOT NULL, "\
                  " Trader TEXT NOT NULL,"\
                  " Symbol TEXT NOT NULL,"\
                  " Quantity INT,"\
                  " Notional INT,"\
                  " TradePrices FLOAT,"\
                  " Low FLOAT, "\
                  " High FLOAT, "\
                  " FOREIGN KEY (Trader) REFERENCES equity_prices(symbol_id))"
    return fat_fingers
