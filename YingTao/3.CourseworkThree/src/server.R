#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Ying Tao
# Date: 2022-01-31
# Topic : Coursework Three, Shiny App server
#--------------------------------------------------------------------------------------

library(ggplot2) 

#Shiny App Step Two---SERVER
server <- function(input, output){
  # Filter data based on selections
  output$tableOne <- renderDataTable(DT::datatable({
    dataOne <- PortfolioPosition_merged
    if(input$date!="All"){
      dataOne <- dataOne[dataOne$cob_date == input$date,]
    }
    if(input$trader!="All"){
      dataOne <- dataOne[dataOne$trader == input$trader,]
    }
    dataOne
  }))
  output$tableTwo <- renderDataTable(DT::datatable({
    dataTwo <- PortfolioPosition
    if(input$date!="All"){
      dataTwo <- dataTwo[dataTwo$cob_date == input$date,]
    }
    if(input$trader!="All"){
      dataTwo <- dataTwo[dataTwo$trader == input$trader,]
    }
    dataTwo
  }))
  
}














