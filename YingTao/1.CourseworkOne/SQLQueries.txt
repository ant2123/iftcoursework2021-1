/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Query 1 for SQL
*/------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--1.a This SELECT statement returns only the columns needed from the table equity_prices. WHERE is greater than: this query filters upon the condition that the high is greater than 500. 
SELECT symbol_id, cob_date, high FROM equity_prices WHERE high > 500;

--1.b WHERE is not null AND another condition is also used to filter out
SELECT limit_id FROM trader_limits
WHERE limit_end IS NOT NULL
AND currency = 'USD';


/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Query 2 for SQL
*/------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--2.a GROUP BY query with functions ROUND, SUM, AVG, MIN and MAX in order to summarise the min, max, total and the average aoumnt issued by the DMO in each auction
SELECT SUM(high) AS sum_amount,
ROUND(AVG(high), 1) AS mean_amount, 
MIN(high), MAX(high) 
FROM equity_prices
WHERE symbol_id = 'A'     	 
GROUP BY cob_date
ORDER BY sum_amount DESC 		
LIMIT 5; 	


/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
